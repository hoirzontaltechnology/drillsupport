#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/qserialport.h>
#include <qhash.h>
#include <qlineedit.h>
#include <qbytearray.h>
#include <qqueue.h>

#define RECORD_BEGIN 0x02454d
#define RECORD_END 0x0304
#define BROADCASTID 0x4243

#define DOWNHOLE_CONTROLLER 10
#define WIRELINE_TRANSCEIVER 30
#define PRIMARY_STEERING_INSTRUMENT 40
#define SECONDARY_STEERING_INSTRUMENT 100
#define GYRO_SURVEY_INSTRUMENT 70
#define PRESSURE_MODULE 130


#define STARTUP 0
#define MOTION_STATUS 5
#define FIRMWARE_VERSION 10
#define HARDWARE_VERSION 11
#define TOOL_SERIALNUMBER 12
#define MODULE_INITIALIZED 15
#define VOLTAGE_DATA 17
#define GENERIC_MESSAGE 19
#define START_MAG_SURVEY 40
#define START_SECONDARY_MAG_SURVEY 100
#define MAG_SURVEY_DATA 41
#define MAG_SURVEY_FAILED 42
#define START_CONTINUOUS_INC_AZM 44
#define CONTINUOUS_SURVEY_DATA 45
#define START_SHOCK_AND_VIBE_ACQ 50
#define SHOCK_AND_VIBE_DATA 51
#define SHOCK_AND_VIBE_DATA_FAILED 52
#define START_GRYO_SURVEY 70
#define GYRO_SURVEY_DATA 71
#define START_PRESSURE_ACQUISITION 130
#define PRESSURE_DATA 131

#define TEMPERATURE_DATA 103

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void timerEvent(QTimerEvent* event);

    QByteArray getBootMessage(unsigned char device);
    QByteArray getFirmwareMessage(unsigned char dev, unsigned char v1, unsigned char v2, unsigned char v3, unsigned char v4);
    QByteArray getHardwareMessage(unsigned char dev, unsigned char v1, unsigned char v2, unsigned char v3, unsigned char v4);
    QByteArray getInitializedMessage(unsigned char dev, bool status);
    QByteArray getStartMagAcqMessage(bool primary);
    QByteArray getMagBroadcastMessage(bool primary, float inc, float azm, float dipa, float mtf, float gtf, float magtotal, float gravtotal,
                                      float ax, float ay, float az, float mx, float my, float mz);
    QByteArray getStartPressureAcqMessage();
    QByteArray getPressureBroadcastMessage();
    QByteArray getPressureBroadcastMessage(float drillpressure, float annularpressure);
    QByteArray getStartContinuousAcqMessage();
    QByteArray getContinuiousBroadcastMessage();
    QByteArray getContinuiousBroadcastMessage(float cinc, float cazm);
    QByteArray getVoltageBroadcastMessage();
    QByteArray getVoltageBroadcastMessage(float voltage);
    QByteArray getTempBroadcastMessage();
    QByteArray getTempBroadcastMessage(float annulartemp, float drilltemp);
    QByteArray getToolSerialNumMessage(QString serial);
    QByteArray getShockAndVibeMessage();
    QByteArray getShockAndVibeMessage(unsigned char axialshocklevel, unsigned char radialshocklevel,
                                      unsigned char axialviblevel, unsigned char radialviblevel,
                                      unsigned char axialviberms, unsigned char radialviberms,
                                      unsigned int axialshockcount, unsigned int radialshockcount,
                                      unsigned char axialmaxshock, unsigned char radialmaxshock,
                                      float rpm);
    QByteArray getMotionStatusMessage();
    QByteArray getMotionStatusMessage(bool flowstate, bool rotationstate);
    QByteArray getErrorMessage();
    QByteArray getInCompleteMessage();

    QByteArray getContinuiousBroadcastMessageEx();
    QByteArray getContinuiousBroadcastMessageEx(float cinc, float cazm,
                                                unsigned char axialshocklevel, unsigned char radialshocklevel,
                                                  unsigned char axialviblevel, unsigned char radialviblevel,
                                                  unsigned char axialviberms, unsigned char radialviberms,
                                                  unsigned int axialshockcount, unsigned int radialshockcount,
                                                  unsigned char axialmaxshock, unsigned char radialmaxshock,
                                                  float rpm);


    QByteArray wrapMessage(QByteArray message);
    unsigned short ComputeCRC16(unsigned char* buf, unsigned len);

    QByteArray swap16(unsigned short value);
    QByteArray swap32(unsigned int value);
    QByteArray swapFloat(float value);


    QByteArray getMagBroadcastMessage(bool primary);

private slots:
    void on_flow_clicked(bool checked);

    void valueChanged(int value);

    void on_start_clicked();

    void on_stop_clicked();

private:
    Ui::MainWindow *ui;
    QSerialPort* m_serialPort;
    QHash<QWidget*,QLineEdit*> m_controlMap;
    QHash<QWidget*,QString> m_scaleMap;
    int m_timerId;

    QQueue<QByteArray> m_messageQueue;
    long m_timeStampOffset;
    unsigned int m_errorGenCounter;
};

#endif // MAINWINDOW_H
