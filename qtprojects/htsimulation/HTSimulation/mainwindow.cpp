#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtSerialPort/QSerialPort>
#include <qdatetime.h>
#include <qthread.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_controlMap.insert(ui->incslider, ui->inc);
    m_scaleMap.insert(ui->incslider, "180,90,2");

    m_scaleMap.insert(ui->azmslider, "360,220,0");
    m_controlMap.insert(ui->azmslider, ui->azm);

    m_scaleMap.insert(ui->axslider, "1,0.5,6");
    m_controlMap.insert(ui->axslider, ui->ax);

    m_scaleMap.insert(ui->ayslider, "1,0.5,6");
    m_controlMap.insert(ui->ayslider, ui->ay);

    m_scaleMap.insert(ui->azslider, "1,0.5,6");
    m_controlMap.insert(ui->azslider, ui->az);

    m_scaleMap.insert(ui->mxslider, "0.5,0.2,6");
    m_controlMap.insert(ui->mxslider, ui->mx);

    m_scaleMap.insert(ui->myslider, "0.5,0.2,6");
    m_controlMap.insert(ui->myslider, ui->my);

    m_scaleMap.insert(ui->mzslider, "0.5,0.2,6");
    m_controlMap.insert(ui->mzslider, ui->mz);

    m_scaleMap.insert(ui->mtfslider, "360,220,0");
    m_controlMap.insert(ui->mtfslider, ui->mtf);

    m_scaleMap.insert(ui->gtfslider, "360,220,0");
    m_controlMap.insert(ui->gtfslider, ui->gtf);

    m_scaleMap.insert(ui->totalgslider, "1.2,0.998,6");
    m_controlMap.insert(ui->totalgslider, ui->totalg);

    m_scaleMap.insert(ui->totalmslider, "0.6,0.499,6");
    m_controlMap.insert(ui->totalmslider, ui->totalm);

    m_scaleMap.insert(ui->dipaslider, "90,57,1");
    m_controlMap.insert(ui->dipaslider, ui->dipa);

    m_scaleMap.insert(ui->annularpressureslider, "1000,400,1");
    m_controlMap.insert(ui->annularpressureslider, ui->annularpressure);

    m_scaleMap.insert(ui->drillpressureslider, "2000,500,1");
    m_controlMap.insert(ui->drillpressureslider, ui->drillpressure);

    m_scaleMap.insert(ui->cincslider, "180,85,1");
    m_controlMap.insert(ui->cincslider, ui->cinc);

    m_scaleMap.insert(ui->cazmslider, "360,50,1");
    m_controlMap.insert(ui->cazmslider, ui->cazm);

    m_scaleMap.insert(ui->drilltempslider, "200,25,1");
    m_controlMap.insert(ui->drilltempslider, ui->drilltemp);

    m_scaleMap.insert(ui->annulartempslider, "200,30,1");
    m_controlMap.insert(ui->annulartempslider, ui->annulartemp);

    m_scaleMap.insert(ui->voltageslider, "30,24,1");
    m_controlMap.insert(ui->voltageslider, ui->voltage);

    m_scaleMap.insert(ui->axialshocklevelslider, "7,1,0");
    m_controlMap.insert(ui->axialshocklevelslider, ui->axialshocklevel);

    m_scaleMap.insert(ui->radialshocklevelslider, "7,2,0");
    m_controlMap.insert(ui->radialshocklevelslider, ui->radialshocklevel);

    m_scaleMap.insert(ui->axialvibelevelslider, "7,2,0");
    m_controlMap.insert(ui->axialvibelevelslider, ui->axialvibelevel);

    m_scaleMap.insert(ui->radialvibleveleslider, "7,2,0");
    m_controlMap.insert(ui->radialvibleveleslider, ui->radialvibelevel);

    m_scaleMap.insert(ui->axialvibermsslider, "50,10,0");
    m_controlMap.insert(ui->axialvibermsslider, ui->axialviberms);

    m_scaleMap.insert(ui->radialvibermsslider, "50,11,0");
    m_controlMap.insert(ui->radialvibermsslider, ui->radialviberms);

    m_scaleMap.insert(ui->axialshockcountslider, "100,20,0");
    m_controlMap.insert(ui->axialshockcountslider, ui->axialshockcount);

    m_scaleMap.insert(ui->radialshockcountslider, "100,20,0");
    m_controlMap.insert(ui->radialshockcountslider, ui->radialshockcount);

    m_scaleMap.insert(ui->rpmslider, "50,21,0");
    m_controlMap.insert(ui->rpmslider, ui->rpm);

    QList<QWidget*> keys = m_scaleMap.keys();

    for(int i=0;i<keys.size();i++)
    {
        QString scalestr = m_scaleMap.value(keys.at(i));
        QStringList parts = scalestr.split(',');
        float fullscale = parts.first().trimmed().toFloat();
        float initialvalue = parts.at(1).trimmed().toFloat();
        int slidervalue = (initialvalue/fullscale) * 100;

        if(m_controlMap.contains(keys.at(i)))
        {
            QSlider* pslider = (QSlider*)keys.at(i);

            pslider->setValue(slidervalue);
            m_controlMap.value(keys.at(i))->setText(parts.at(1));
        }

        connect(keys.at(i), SIGNAL(valueChanged(int)), this, SLOT(valueChanged(int)));
    }

    m_timerId = -1;

    m_serialPort = new QSerialPort("COM1");
    m_serialPort->setBaudRate(QSerialPort::Baud115200);

    bool status = m_serialPort->open(QSerialPort::WriteOnly);

    ui->start->setEnabled(status);
    ui->stop->setEnabled(status);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::timerEvent(QTimerEvent* event)
{
    if(!m_messageQueue.isEmpty())
    {
        int value = ((float)rand())/ (RAND_MAX/20);
        QByteArray junk;

        for(int i=0;i<value;i++)
        {
            junk.append(i);
        }

        m_serialPort->write(junk);

        QByteArray message = wrapMessage(m_messageQueue.dequeue());

//        if((m_errorGenCounter++ % 5) == 0)
//        {
//            int modindex = message.size() - 1 - 5;

//            message.insert(modindex, message.at(modindex)+1);

//        }
//        m_serialPort->write(message);

        QByteArray tmp1 = message.mid(0, 10);
        QByteArray tmp2 = message.mid(10);

        m_serialPort->write(tmp1);
        m_serialPort->waitForBytesWritten(500);
        QThread::currentThread()->msleep(100);
        m_serialPort->write(tmp2);

        if(m_messageQueue.isEmpty())
        {
            m_messageQueue.enqueue(getMagBroadcastMessage(true));
            m_messageQueue.enqueue(getContinuiousBroadcastMessage());
            m_messageQueue.enqueue(getVoltageBroadcastMessage());
            m_messageQueue.enqueue(getTempBroadcastMessage());
            m_messageQueue.enqueue(getPressureBroadcastMessage());
            m_messageQueue.enqueue(getMotionStatusMessage());
            m_messageQueue.enqueue(getShockAndVibeMessage());
            m_messageQueue.enqueue(getInCompleteMessage());
        }
    }
    else
    {

    }
}

void MainWindow::on_flow_clicked(bool checked)
{

}


void MainWindow::valueChanged(int value)
{
   QWidget* current = QApplication::focusWidget();

   if(m_controlMap.contains(current))
   {
       QLineEdit* pedit = m_controlMap.value(current);
       QString scalingstr = m_scaleMap.value(current);
       QStringList parts = scalingstr.split(',');
       float fullscale = parts.first().trimmed().toFloat();
       int res = parts.last().trimmed().toInt();
       float scaled = fullscale * (value/100.0);

       pedit->setText(QString::number(scaled, 'f', res));
   }
}

void MainWindow::on_start_clicked()
{
    if(m_timerId == -1)
    {
        m_timerId = startTimer(1000);
        m_timeStampOffset = QDateTime::currentDateTime().toTime_t();


        m_messageQueue.clear();
        m_messageQueue.enqueue(getBootMessage(5));
        m_messageQueue.enqueue(getBootMessage(6));
        m_messageQueue.enqueue(getBootMessage(7));
        m_messageQueue.enqueue(getBootMessage(DOWNHOLE_CONTROLLER));
        m_messageQueue.enqueue(getFirmwareMessage(DOWNHOLE_CONTROLLER, 1, 2, 3, 5));
        m_messageQueue.enqueue(getHardwareMessage(DOWNHOLE_CONTROLLER, 5, 6, 7, 8));
        m_messageQueue.enqueue(getInitializedMessage(DOWNHOLE_CONTROLLER, true));
        m_messageQueue.enqueue(getStartMagAcqMessage(true));
        m_messageQueue.enqueue(getStartPressureAcqMessage());
        m_messageQueue.enqueue(getStartContinuousAcqMessage());
        m_messageQueue.enqueue(this->getToolSerialNumMessage("bob1"));

        ui->start->setEnabled(false);
        ui->stop->setEnabled(true);

        QByteArray garbage;

        for(int i=0;i<30;i++)
        {
            garbage.append(i);
        }

        m_serialPort->clear();
        m_serialPort->write(garbage);

        m_errorGenCounter = 0;
    }
}

void MainWindow::on_stop_clicked()
{
    killTimer(m_timerId);
    m_timerId = -1;
    ui->start->setEnabled(true);
    ui->stop->setEnabled(false);

}


QByteArray MainWindow::getBootMessage(unsigned char device)
{
    QByteArray message;

    message.append(swap16(0x4243));
    message.append(swap16(STARTUP));
    message.append(device);
    message.append(0x04);
    message.append("BOOT");

    return message;
}

QByteArray MainWindow::getFirmwareMessage(unsigned char dev, unsigned char v1, unsigned char v2, unsigned char v3, unsigned char v4)
{
    QByteArray message;

    message.append(swap16(0x4243));
    message.append(swap16(FIRMWARE_VERSION));
    message.append(dev);
    message.append(v1);
    message.append(v2);
    message.append(v3);
    message.append(v4);

    return message;
}
QByteArray MainWindow::getHardwareMessage(unsigned char dev, unsigned char v1, unsigned char v2, unsigned char v3, unsigned char v4)
{
    QByteArray message;

    message.append(swap16(0x4243));
    message.append(swap16(HARDWARE_VERSION));
    message.append(dev);
    message.append(v1);
    message.append(v2);
    message.append(v3);
    message.append(v4);

    return message;
}
QByteArray MainWindow::getInitializedMessage(unsigned char dev, bool status)
{
    QByteArray message;

    message.append(swap16(0x4243));
    message.append(swap16(MODULE_INITIALIZED));
    message.append(dev);
    message.append(status);

    return message;
}


QByteArray MainWindow::getStartMagAcqMessage(bool primary)
{
    QByteArray message;
    unsigned short acqvalue = primary ? START_MAG_SURVEY:START_SECONDARY_MAG_SURVEY;
    QString msg = primary ? "Primary Mag Acq Started" : "Secondary Mag Acq Started";
    unsigned char device = primary ? PRIMARY_STEERING_INSTRUMENT:SECONDARY_STEERING_INSTRUMENT;

    message.append(swap16(0x4243));
    message.append(swap16(acqvalue));
    message.append(device);
    message.append(msg.size());
    message.append(msg);

    return message;
}

QByteArray MainWindow::getMagBroadcastMessage(bool primary)
{
    float inc = ui->inc->text().toFloat();
    float azm = ui->azm->text().toFloat();
    float dipa = ui->dipa->text().toFloat();
    float mtf = ui->mtf->text().toFloat();
    float gtf = ui->gtf->text().toFloat();
    float magtotal = ui->totalm->text().toFloat();
    float gravtotal = ui->totalg->text().toFloat();
    float ax = ui->ax->text().toFloat();
    float ay = ui->ay->text().toFloat();
    float az = ui->az->text().toFloat();
    float mx = ui->mx->text().toFloat();
    float my = ui->my->text().toFloat();
    float mz = ui->mz->text().toFloat();

    return getMagBroadcastMessage(primary, inc, azm, dipa, mtf, gtf, magtotal, gravtotal, ax, ay, az, mx, my, mz);
}

QByteArray MainWindow::getMagBroadcastMessage(bool primary, float inc, float azm, float dipa, float mtf, float gtf, float magtotal, float gravtotal,
                                  float ax, float ay, float az, float mx, float my, float mz)
{
    QByteArray message;
    message.append(swap16(0x4243));
    message.append(swap16(MAG_SURVEY_DATA));
    message.append((unsigned char)DOWNHOLE_CONTROLLER);
    message.append(swapFloat(inc));
    message.append(swapFloat(azm));
    message.append(swapFloat(mtf));
    message.append(swapFloat(gtf));
    message.append(swapFloat(magtotal));
    message.append(swapFloat(gravtotal));
    message.append(swapFloat(dipa));
    message.append(swapFloat(ax));
    message.append(swapFloat(ay));
    message.append(swapFloat(az));
    message.append(swapFloat(mx));
    message.append(swapFloat(my));
    message.append(swapFloat(mz));

    return message;
}
QByteArray MainWindow::getStartPressureAcqMessage()
{
    QByteArray message;
    QString msg = "Start Pressure Acq";

    message.append(swap16(0x4243));
    message.append(swap16(START_PRESSURE_ACQUISITION));
    message.append((unsigned char)PRESSURE_MODULE);
    message.append(msg.size());
    message.append(msg);

    return message;
}


QByteArray MainWindow::getShockAndVibeMessage()
{
   unsigned char axialshocklevel = ui->axialshocklevel->text().toUShort();
   unsigned char radialshocklevel = ui->radialshocklevel->text().toUShort();
   unsigned char axialvibelevel = ui->axialvibelevel->text().toUShort();
   unsigned char radialvibelevel = ui->radialvibelevel->text().toUShort();
   unsigned char axialviberms = ui->axialviberms->text().toUShort();
   unsigned char radialviberms = ui->radialviberms->text().toUShort();
   unsigned int axialshockcount = ui->axialshockcount->text().toUShort();
   unsigned int radialshockcount = ui->radialshockcount->text().toUShort();
   unsigned char axialmaxshock = 21;
   unsigned char radialmaxshock = 23;
   float rpm = ui->rpm->text().toFloat();

   return getShockAndVibeMessage(axialshocklevel, radialshocklevel, axialvibelevel, radialvibelevel,
                                 axialviberms, radialviberms, axialshockcount, radialshockcount,
                                 axialmaxshock, radialmaxshock, rpm);
}

QByteArray MainWindow::getShockAndVibeMessage(unsigned char axialshocklevel, unsigned char radialshocklevel,
                                  unsigned char axialviblevel, unsigned char radialviblevel,
                                  unsigned char axialviberms, unsigned char radialviberms,
                                  unsigned int axialshockcount, unsigned int radialshockcount,
                                  unsigned char axialmaxshock, unsigned char radialmaxshock,
                                  float rpm)
{
    QByteArray message;

    message.append(swap16(0x4243));
    message.append(swap16(SHOCK_AND_VIBE_DATA));
    message.append((unsigned char)DOWNHOLE_CONTROLLER);
    message.append(axialshocklevel);
    message.append(radialshocklevel);
    message.append(axialviblevel);
    message.append(radialviblevel);
    message.append(axialviberms);
    message.append(radialviberms);
    message.append(swap16(axialshockcount));
    message.append(swap16(radialshockcount));
    message.append(axialmaxshock);
    message.append(radialmaxshock);
    message.append(swapFloat(rpm));


    return message;
}

QByteArray MainWindow::getContinuiousBroadcastMessageEx()
{
    unsigned char axialshocklevel = ui->axialshocklevel->text().toUShort();
    unsigned char radialshocklevel = ui->radialshocklevel->text().toUShort();
    unsigned char axialvibelevel = ui->axialvibelevel->text().toUShort();
    unsigned char radialvibelevel = ui->radialvibelevel->text().toUShort();
    unsigned char axialviberms = ui->axialviberms->text().toUShort();
    unsigned char radialviberms = ui->radialviberms->text().toUShort();
    unsigned int axialshockcount = ui->axialshockcount->text().toUShort();
    unsigned int radialshockcount = ui->radialshockcount->text().toUShort();
    unsigned char axialmaxshock = 21;
    unsigned char radialmaxshock = 23;
    float rpm = ui->rpm->text().toFloat();

    return getContinuiousBroadcastMessageEx(ui->cinc->text().toFloat(), ui->cazm->text().toFloat(),
                                            axialshocklevel, radialshocklevel, axialvibelevel, radialvibelevel,
                                          axialviberms, radialviberms, axialshockcount, radialshockcount,
                                          axialmaxshock, radialmaxshock, rpm);

}

QByteArray MainWindow::getContinuiousBroadcastMessageEx(float cinc, float cazm,
                                            unsigned char axialshocklevel, unsigned char radialshocklevel,
                                              unsigned char axialviblevel, unsigned char radialviblevel,
                                              unsigned char axialviberms, unsigned char radialviberms,
                                              unsigned int axialshockcount, unsigned int radialshockcount,
                                              unsigned char axialmaxshock, unsigned char radialmaxshock,
                                              float rpm)
{
    QByteArray message;
    message.append(swap16(0x4243));
    message.append(swap16(CONTINUOUS_SURVEY_DATA));
    message.append((unsigned char)DOWNHOLE_CONTROLLER);
    message.append(swapFloat(cinc));
    message.append(swapFloat(cazm));
    message.append(axialshocklevel);
    message.append(radialshocklevel);
    message.append(axialviblevel);
    message.append(radialviblevel);
    message.append(axialviberms);
    message.append(radialviberms);
    message.append(swap16(axialshockcount));
    message.append(swap16(radialshockcount));
    message.append(axialmaxshock);
    message.append(radialmaxshock);
    message.append(swapFloat(rpm));

    return message;
}


QByteArray MainWindow::getMotionStatusMessage()
{
    bool flowstate = ui->flow->isChecked();
    bool rotatestate = ui->rotation->isChecked();

    return getMotionStatusMessage(flowstate, rotatestate);
}

QByteArray MainWindow::getMotionStatusMessage(bool flowstate, bool rotationstate)
{
    QByteArray message;
    message.append(swap16(0x4243));
    message.append(swap16(MOTION_STATUS));
    message.append((unsigned char)DOWNHOLE_CONTROLLER);
    message.append((unsigned char)flowstate);
    message.append((unsigned char) rotationstate);

    return message;
}

QByteArray MainWindow::getPressureBroadcastMessage()
{
    return getPressureBroadcastMessage(ui->drillpressure->text().toFloat(), ui->annularpressure->text().toFloat());
}

QByteArray MainWindow::getPressureBroadcastMessage(float drillpressure, float annularpressure)
{
    QByteArray message;

    message.append(swap16(0x4243));
    message.append(swap16(PRESSURE_DATA));
    message.append((unsigned char)DOWNHOLE_CONTROLLER);
    message.append(swapFloat(annularpressure));
    message.append(swapFloat(drillpressure));

    return message;
}
QByteArray MainWindow::getStartContinuousAcqMessage()
{
    QByteArray message;
    QString msg = "Start Continuous Acq";

    message.append(swap16(0x4243));
    message.append(swap16(START_CONTINUOUS_INC_AZM));
    message.append((unsigned char)PRIMARY_STEERING_INSTRUMENT);
    message.append(msg.size());
    message.append(msg);

    return message;
}

QByteArray MainWindow::getContinuiousBroadcastMessage()
{
    return getContinuiousBroadcastMessage(ui->cinc->text().toFloat(), ui->cazm->text().toFloat());
}

QByteArray MainWindow::getContinuiousBroadcastMessage(float cinc, float cazm)
{
    QByteArray message;

    message.append(swap16(0x4243));
    message.append(swap16(CONTINUOUS_SURVEY_DATA));
    message.append((unsigned char)DOWNHOLE_CONTROLLER);
    message.append(swapFloat(cinc));
    message.append(swapFloat(cazm));

    return message;
}

QByteArray MainWindow::getVoltageBroadcastMessage()
{
    return getVoltageBroadcastMessage(ui->voltage->text().toFloat());
}

QByteArray MainWindow::getVoltageBroadcastMessage(float voltage)
{
    QByteArray message;

    message.append(swap16(0x4243));
    message.append(swap16(VOLTAGE_DATA));
    message.append((unsigned char)DOWNHOLE_CONTROLLER);
    message.append(swapFloat(voltage));

    return message;
}

QByteArray MainWindow::getTempBroadcastMessage()
{
    return getTempBroadcastMessage(ui->annulartemp->text().toFloat(), ui->drilltemp->text().toFloat());
}

QByteArray MainWindow::getTempBroadcastMessage(float annulartemp, float drilltemp)
{
    QByteArray message;

    message.append(swap16(0x4243));
    message.append(swap16(TEMPERATURE_DATA));
    message.append((unsigned char)DOWNHOLE_CONTROLLER);
    message.append(swapFloat(annulartemp));
    message.append(swapFloat(drilltemp));

    return message;
}

QByteArray MainWindow::getToolSerialNumMessage(QString serial)
{
    QByteArray message;

    message.append(swap16(BROADCASTID));
    message.append(swap16(TOOL_SERIALNUMBER));
    message.append((unsigned char)DOWNHOLE_CONTROLLER);
    message.append(serial.size());
    message.append(serial.toLatin1());

    return message;
}

QByteArray MainWindow::getErrorMessage()
{
    QByteArray message;

    return message;
}

QByteArray MainWindow::getInCompleteMessage()
{
    QByteArray message = getShockAndVibeMessage();

    return message.mid(0, message.size()/2);
}

QByteArray MainWindow::wrapMessage(QByteArray message)
{
    QByteArray wrapped;
    unsigned int timestamp = QDateTime::currentDateTime().toTime_t() - m_timeStampOffset;

    wrapped.append(0x02);   // header
    wrapped.append(0x45);
    wrapped.append(0x4d);
    wrapped.append(message.size() + 4); // message length - does not include the header and length byte - does not include CRC and footer
    wrapped.append(swap32(timestamp));
    wrapped.append(message);

    unsigned char* pdata = (unsigned char*) wrapped.data();
    unsigned short crc = ComputeCRC16(pdata, wrapped.size());

    wrapped.append(swap16(crc));   // add CRC
    wrapped.append(0x03);           // add footer
    wrapped.append(0x04);

    return wrapped;
}


unsigned short MainWindow::ComputeCRC16(unsigned char* buf, unsigned len)
{
    unsigned short crc = 0;

    for (unsigned int j = 0; j < len; j++)
    {
        unsigned char b = buf[j];
        for (unsigned char i = 0; i < 8; i++)
        {
            crc = ((b ^ (unsigned)crc) & 1) ? ((crc >> 1) ^ 0xA001) : (crc >> 1);
            b >>= 1;

        }
    }

    return crc;
}


QByteArray MainWindow::swap16(unsigned short value)
{
   QByteArray bytes;
   unsigned char* p = (unsigned char*) &value;

   bytes.append(*(p + 1));
   bytes.append(*p);
   return bytes;
}

QByteArray MainWindow::swap32(unsigned int value)
{
    QByteArray bytes;
    unsigned char* p = (unsigned char*) &value;

    bytes.append(*(p + 3));
    bytes.append(*(p + 2));
    bytes.append(*(p + 1));
    bytes.append(*p);
    return bytes;
}

QByteArray MainWindow::swapFloat(float value)
{
    QByteArray bytes;
    unsigned char* p = (unsigned char*) &value;

    bytes.append(*(p + 3));
    bytes.append(*(p + 2));
    bytes.append(*(p + 1));
    bytes.append(*p);
    return bytes;
}

