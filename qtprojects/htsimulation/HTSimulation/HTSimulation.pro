#-------------------------------------------------
#
# Project created by QtCreator 2017-07-19T20:54:21
#
#-------------------------------------------------

QT       += core gui  serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HTSimulation
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
