#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>
#include <qdatetime.h>
#include <qlist.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    m_pSerialPort = new QSerialPort("COM8");
//    m_pSerialPort = new QSerialPort("COM3");
    m_pSerialPort = new QSerialPort("COM2");

    connect(m_pSerialPort, SIGNAL(readyRead()), this, SLOT(dataAvailable()));

//    m_pSerialPort->setBaudRate(QSerialPort::Baud1200);
    m_pSerialPort->setBaudRate(QSerialPort::Baud115200);
    bool status = m_pSerialPort->open(QSerialPort::ReadOnly);

    qDebug() << "port status = " << status << " error = " << m_pSerialPort->errorString();

//    QList<QString> tooldata;

//    tooldata << "23b3988a0d0a4300c3ffbf81ac80c7ffe3c09880a380d4ffd8fff7ffb10d0a4301bef99e80f1ff9480a7c09280a080d7ffcdfffcffce0d0a4302c7f7eb80c780a5ffacc094809980e0ffddffbe80e50d0a4303fcfdcf81faff88808aced280cdffc1808080feffbd0d0a4304d5feb281e5ffa280facdd080c4ffcd8082808780fb0d0a43058bff8b81f0ff9580f0cfc480c4ffcd80fdff8c80f40d0a4306808080808080808080808080bf0d0a4307808080808080808080808080bf0d0a4308808080808080808080808080bf0d0a4980b2b0b0b7b5808080808080808080808080808080808080808080808080808cb3b0b6afb1b2afb2b0b1b48080808080b1b0bab2b7bab4b38080808080808087f88e0d0a";
//    tooldata << "59a08ca8bbcba6e790bdc9ccd683b0df92d5c1d6d199b1a9b6e80d0a4732353638333030303030bd0d0a5032353638333030324334830d0a4132373131433630303030fe0d0a";
//    tooldata << "59a28ca880cba6c990bd9cccd6a0b0df87d5c1fcd0998ca9b6f60d0a4730423541463930303030de0d0a5030423541463930304446bd0d0a4130423542313030303030f60d0a";

//    for(int i=0;i<tooldata.size();i++)
//    {
//        char* buffer = convertHexString(tooldata.at(i));
//        QString msg;

//        for(int j=0;j<10;j++)
//        {
//            QString numberrep = QString::number((unsigned char)buffer[j], 16);

//            numberrep = numberrep.size() == 1 ? QString("0")+numberrep:numberrep;
//            msg += numberrep;
//        }

//        qDebug() << "Buffer beginning = " << msg;

//        HTData::parseBuffer(buffer, msg.size());

//        free(buffer);
//    }

//    testHTData();

    testFloatConversion();
}

MainWindow::~MainWindow()
{
    delete ui;
}

char* MainWindow::convertHexString(QString str)
{
    char* buffer = (char*) malloc(str.size());
    int index = 0;

    for(int i=0;i<str.size()-2;)
    {
        QString temp = str.mid(i,2);
        unsigned char value = temp.toInt(nullptr, 16);

        buffer[index++] = value;
        i += 2;
    }

    return buffer;
}

void MainWindow::dataAvailable()
{
    long long currenttime = QDateTime::currentMSecsSinceEpoch();
    long long delta = currenttime - m_lastReadTime;
    unsigned int used = 0;

    while(m_pSerialPort->bytesAvailable() > 0)
    {
        m_bytes.append(m_pSerialPort->readAll());
    }


//    qDebug() << "!!!!read this many bytes " << m_pSerialPort->bytesAvailable();

//    if((delta > 100))
    {
        QString msg = QString(m_bytes);
        QString hexmsg = QString(m_bytes.toHex());

        qDebug() << "Received these bytes at this time " << QDateTime::currentDateTime().toString() << " content " << msg << "\t\t\t\t\thex version " << hexmsg;

//        if(m_bytes.size() > 15)
        {
            QList<QByteArray> messagelist = HT_ToolParser::splitbuffers(m_bytes.data(), m_bytes.size(), &used);

            for(int i=0;i<messagelist.size();i++)
            {
                QByteArray msg = messagelist.at(i);

                m_receivedData.append(msg);

                qDebug() << "       handing this individual message " << QString(m_bytes.toHex());

                QHash<QString,QString> attributes =  HT_ToolParser::parseRawMessage(msg.data(), msg.size());
                QString toolmsg;

//                used += msg.size();

                if(attributes.contains(RESPONSE_CONTENT))
                {
                    QString content = attributes.value(RESPONSE_CONTENT);

                    toolmsg = content;
                    qDebug() << " msg content = " << content;
                }
                else
                {
                    qDebug() << "!!!! no content found";
                    toolmsg = "not recognized";
                }

                QString text = ui->datadisplay->toPlainText();

                text += "\n" + toolmsg;
                ui->datadisplay->setPlainText(text);
            }

        }

        if(used < m_bytes.size())
        {
//            qDebug() << "purging receive buffer size = " << m_bytes.size() << " Tossing this many " << used;
            m_bytes = m_bytes.mid(used);
        }
        else
        {
            m_bytes.clear();
//            qDebug() << "clearing entire receive buffer";
        }
    }


    m_lastReadTime = currenttime;

//    while(m_pSerialPort->bytesAvailable() > 0)
//    {
//        m_bytes.append(m_pSerialPort->readAll());
//    }

//    qDebug() << "read this many bytes " << m_bytes.size();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    qDebug() << "Closing up shop - got this many messages " << m_receivedData.size();

    for(int i=0;i<m_receivedData.size();i++)
    {
        qDebug() << QString(m_receivedData.at(i).toHex());
    }

    event->accept();
}


void MainWindow::testFloatConversion()
{
    unsigned char testdata[] = {0x00, 0x29, 0x28, 0x42, 0xb3, 0x67, 0xc7, 0x43};
    float val1 = HT_ToolParser::swapFloat(testdata);
    float val2 = HT_ToolParser::swapFloat(testdata + 4);

    qDebug() << "value1 converted to " << val1 << " value2 converted to " << val2;
}

double HTData::convertRawAxis(char hibyte, char middlebyte, char lobyte)
{
    double hi;
    double mid;
    double lo;
    double result;

    hi = ((unsigned char)hibyte - 128) * 16384;
    mid = ((unsigned char)middlebyte - 128) * 128;
    lo = (unsigned char)lobyte - 128;

    result = 1048576 - (hi + mid + lo);
    result = result * 5000.0 / 1048576.0;
    qDebug() << "convertRawAxis hi = " << (unsigned char) hibyte << " middle = " << (unsigned char)middlebyte << " lo = " << (unsigned char)lobyte << " result = " << result;

    return result;
}

double HTData::convertCoeffecient(char hibyte, char lobyte)
{
    return (hibyte - 128) + ((lobyte - 128) * 128);
}


long HTData::calcRecordCheckSum(char* pstart, int count, long checksumoffset)
{
    long checksum = 0;

    for(int i=0;i<count;i++)
    {
        checksum += (unsigned char)*(pstart + i);
    }

    return (checksum % 128) + checksumoffset;
}

long HTData::calcCheckSumOffset(char b1, char b2, char b3)
{
    return b1 - (b2 - 128) - (b3 - 128);
}


double HTData::convertRawTemp(double temperature)
{
    return (temperature / 10.0) - 273.2;
}

double HTData::convertRawVoltage(double voltage)
{
    return 48.0 * voltage / 1000.0;
}

double HTData::calcBiasFactor(double biasfactorA0, double biasfactorA1, double biasfactorA2, double biasfactorA3, double tempC)
{
    return (biasfactorA0 / 10) + ((biasfactorA1 * tempC) / 1000) + ((biasfactorA2 * pow(tempC, 2)) / 100000) + ((biasfactorA3 * pow(tempC, 3)) / 10000000);
}

double HTData::calcScaleFactor(double scalefactorA0, double scalefactorA1, double scalefactorA2, double scalefactorA3, double tempC)
{
    return (scalefactorA0 / 10000) + ((scalefactorA1 * tempC) / 1000000) + ((scalefactorA2 * pow(tempC, 2)) / 100000000) + ((scalefactorA3 * pow(tempC, 3)) / 10000000000);

}


double HTData::adjustRawVector(double rawvalue, double scalefactor, double bias,
                                  double crossfactor1, double raw1, double scale1, double bias1,
                                  double crossfactor2, double raw2, double scale2, double bias2)
{
    return ((rawvalue / scalefactor) - bias) -
                (crossfactor1 * ((raw1 / scale1) - bias1) / 10000) -
                (crossfactor2 * ((raw2 / scale2) - bias2) / 10000);
}

void HTData::parseBuffer(char* buffer, int bufferlen)
{
    if(*buffer == '#')
    {
        parseCoeffecientsBuffer(buffer, bufferlen);
    }

    if(*buffer == 'Y')
    {
        parseRawBuffer(buffer, bufferlen);
    }
}


void HTData::parseCoeffecientsBuffer(char* buffer, int bufferlen)
{
    struct HT_Coefficients* cb = (struct HT_Coefficients*)buffer;
    long checksumoffset = calcCheckSumOffset(cb->filler1, cb->filler1, cb->filler1);
    double hxbiasA0 = convertCoeffecient(cb->GX_Bias_Factor_A0_hi, cb->GX_Bias_Factor_A0_lo);
    double hxbiasA1 = convertCoeffecient(cb->GX_Bias_Factor_A1_hi, cb->GX_Bias_Factor_A1_lo);
    double hxbiasA2 = convertCoeffecient(cb->GX_Bias_Factor_A2_hi, cb->GX_Bias_Factor_A2_lo);
    double hxbiasA3 = convertCoeffecient(cb->GX_Bias_Factor_A3_hi, cb->GX_Bias_Factor_A3_lo);
    double hxscaleA0 = convertCoeffecient(cb->GX_Scale_Factor_A0_hi, cb->GX_Scale_Factor_A0_lo);
    double hxscaleA1 = convertCoeffecient(cb->GX_Scale_Factor_A1_hi, cb->GX_Scale_Factor_A1_lo);
    double hxscaleA2 = convertCoeffecient(cb->GX_Scale_Factor_A2_hi, cb->GX_Scale_Factor_A2_lo);
    double hxscaleA3 = convertCoeffecient(cb->GX_Scale_Factor_A3_hi, cb->GX_Scale_Factor_A3_lo);
    double hxcrossfactor1 = convertCoeffecient(cb->GX_Cross_Axis_Factor_K1_hi, cb->GX_Cross_Axis_Factor_K1_lo);
    double hxcrossfactor2 = convertCoeffecient(cb->GX_Cross_Axis_Factor_K2_hi, cb->GX_Cross_Axis_Factor_K2_lo);
    long checksum1 = calcRecordCheckSum((buffer+9), 20, 0);

    qDebug() << "checksumofset = " << checksumoffset << " checksum1 = " << checksum1 << " transmitted checksum = " << (unsigned char)cb->GX_CheckSum;
}

void HTData::parseRawBuffer(char* buffer, int bufferlen)
{
    struct HT_RawAxisData* bf = (struct HT_RawAxisData*)buffer;
    double temperature = convertRawAxis(bf->Temp_hibyte, bf->Temp_middlebyte, bf->Temp_lobyte);
    double battery = convertRawAxis(bf->Voltage_hibyte, bf->Voltage_middlebyte, bf->Voltage_lobyte);

    temperature = convertRawTemp(temperature);
    battery = convertRawVoltage(battery);

    qDebug() << "Temperature = " << temperature << " battery = " << battery;

}


QList<QByteArray> HT_ToolParser::splitbuffers(char* buffer, int bufferlen, unsigned int* consumedcount)
{
    QList<QByteArray> commandlist;
    QByteArray current;
    int i=0;

//    qDebug() << "parsing message with this length " << bufferlen;

    for(i=0;i<bufferlen;i++)        // ugly  - make sure to point start at a boundary - toss the rest.
    {
        if((i+2) < bufferlen)
        {
            if((buffer[i] == 0x02) && (buffer[i+1] == 0x45) && (buffer[i+2] == 0x4d))
            {
                break;
//                qDebug() << "starting point found at index " << i;
            }
        }

    }

    qDebug() << "parsing buffer - starting at index " << i;

    for(;i<bufferlen;i++)
    {
        bool skiptherest = false;

        if((i+2) < bufferlen)
        {
            if((buffer[i] == 0x02) && (buffer[i+1] == 0x45) && (buffer[i+2] == 0x4d))
            {
                skiptherest = true;
//                qDebug() << "Started tool message";
                for(int j=0;j<3;j++)
                {
                    current.append(buffer[i + j]);
                }

                i += 2;
            }
        }
//        else
        if(!skiptherest)
        {
//            qDebug() << " looking at element i = " << i << " its value is 0x" << QString::number((unsigned int)buffer[i], 16);

            if(((i+1) < bufferlen) && (buffer[i] == 0x03) && (buffer[i+1] == 0x04))
            {
                for(int j=0;j<2;j++)
                {
                    current.append(buffer[i + j]);
                }

                i += 1;
                *consumedcount = i + 1;
                qDebug() << "finished tool message with this length " << current.size();
                commandlist.append(current);
                current.clear();
            }
            else
            {
                current.append(buffer[i]);
            }
        }
    }

    return commandlist;
}

QStringList HT_ToolParser::parseBuffer(char* buffer, int bufferlen)
{
    QStringList parseddata;
    QList<QByteArray> commandlist;
    QByteArray current;

    qDebug() << "parsing message with this length " << bufferlen;

    for(int i=0;i<bufferlen;i++)
    {
        bool skiptherest = false;

        if((i+2) < bufferlen)
        {
            if((buffer[i] == 0x02) && (buffer[i+1] == 0x45) && (buffer[i+2] == 0x4d))
            {
                skiptherest = true;
                qDebug() << "Started tool message";
                for(int j=0;j<3;j++)
                {
                    current.append(buffer[i + j]);
                }

                i += 2;
            }
        }
//        else
        if(!skiptherest)
        {
            qDebug() << " looking at element i = " << i << " its value is 0x" << QString::number((unsigned int)buffer[i], 16);

            if(((i+1) < bufferlen) && (buffer[i] == 0x03) && (buffer[i+1] == 0x04))
            {
                for(int j=0;j<2;j++)
                {
                    current.append(buffer[i + j]);
                }

                i += 1;

                qDebug() << "finished tool message with this length " << current.size();
                commandlist.append(current);
                current.clear();
            }
            else
            {
                current.append(buffer[i]);
            }
        }
    }

    qDebug() << "list size = " << commandlist.size();

    for(int i=0;i<commandlist.size();i++)
    {
        qDebug() << "!!!!!step .....";
        QByteArray toolresponse = commandlist.at(i);
        char* pdata = toolresponse.data();
        unsigned long msgid = swap24((unsigned char*)pdata);
        QString msgdesc = QString("0x") +  QString::number(msgid, 16);
        qDebug() << "!!!!!step .....A";
        unsigned short crc = ComputeCRC16((unsigned char*)pdata, toolresponse.size()-4);
        qDebug() << "!!!!!step .....B";
        unsigned short* crccalc = (unsigned short*) (pdata + (toolresponse.size()-4));
        unsigned short crcsent = swap16((unsigned char*)(pdata + (toolresponse.size()-4)));
        QString crcsentstr = QString("0x") + QString::number(crcsent, 16);
        unsigned int timestamp = swap32((unsigned char*)(pdata + 3));   //????
        unsigned int cmdid = swap16((unsigned char*)(pdata + 7));   //????
        unsigned int fcnid = swap16((unsigned char*)(pdata + 9));
        QString timestr = QString::number(timestamp);
        QString cmdstr = QString("0x") + QString::number(cmdid, 16);
        QString fncstr = QString("0x") + QString::number(fcnid, 16);
        qDebug() << "!!!!!step .....C";
        qDebug() << "!!!!!step .....added this message " << "base pointer = 0x" << QString::number((unsigned long)pdata, 16) << " crc pointer = 0x" << QString::number((unsigned long)crccalc, 16);
        QString description = timestr + ":" + getMessageContent(cmdid, fcnid, pdata, toolresponse.size());
        QString msg = QString("message ") + QString::number(i) + QString(" Desc = ") + msgdesc + QString(" timestamp ") + timestr + QString(" msg id = ") + cmdstr + QString(" fcn id = ") + fncstr + QString(" crc = ") + crcsentstr  + QString(" calculated CRC = 0x") + QString::number((unsigned long)crc, 16) + " description = " + description;

        qDebug() << "!!!!!step .....added this message " << msg;


        parseddata.append(msg);

        qDebug() << "!!!!!step .....added this messageA " << msg;

    }

    return parseddata;
}


unsigned short HT_ToolParser::ComputeCRC16(unsigned char* buf, unsigned len)
{
    unsigned short crc = 0;

//    qDebug() << "!!! starting CRC value = 0";
    for (unsigned int j = 0; j < len; j++)
    {
        unsigned char b = buf[j];
        for (unsigned char i = 0; i < 8; i++)
        {
            crc = ((b ^ (unsigned)crc) & 1) ? ((crc >> 1) ^ 0xA001) : (crc >> 1);
            b >>= 1;

        }

//        qDebug() << "  for element " << j << " with a value of 0x " << QString::number((unsigned int)buf[j], 16) << " CRC value = 0x" << QString::number(crc, 16);
    }

//    qDebug() << "  ending CRC = 0x" << QString::number(crc, 16);

    return crc;
}


unsigned int HT_ToolParser::swap16(unsigned char *buffer)
{
    unsigned int retval = 0;

    retval |= (unsigned int) *(buffer+1);
    retval |= ((unsigned int) *buffer) << 8;
    return retval;
}

unsigned long HT_ToolParser::swap32(unsigned char *buffer)
{
    unsigned long retval = 0;

    retval |= (unsigned long) *(buffer+3);
    retval |= ((unsigned long) *(buffer+2)) << 8;
    retval |= ((unsigned long) *(buffer+1)) << 16;
    retval |= ((unsigned long) *(buffer)) << 24;
    return retval;
}

unsigned long HT_ToolParser::swap24(unsigned char *buffer)
{
    unsigned long retval = 0;

    retval |= (unsigned long) *(buffer+2);
    retval |= ((unsigned long) *(buffer+1)) << 8;
    retval |= ((unsigned long) *(buffer)) << 16;
    return retval;
}

float HT_ToolParser::swapFloat(unsigned char* buffer)
{
    float retvalue = 0;
    unsigned long placeholder = 0;
    float* pfloat = (float*) &placeholder;

    placeholder |= (unsigned long) *(buffer+3);
    placeholder |= ((unsigned long) *(buffer+2)) << 8;
    placeholder |= ((unsigned long) *(buffer+1)) << 16;
    placeholder |= ((unsigned long) *(buffer)) << 24;
//    placeholder |= (unsigned long) *(buffer);
//    placeholder |= ((unsigned long) *(buffer+1)) << 8;
//    placeholder |= ((unsigned long) *(buffer+2)) << 16;
//    placeholder |= ((unsigned long) *(buffer+3)) << 24;
    retvalue = *pfloat;

    qDebug() << "conv to Float raw value = 0x" << QString::number(placeholder, 16);

    return retvalue;
}

QString HT_ToolParser::getMessageContent(unsigned int msgid, unsigned int fcnid, char* buffer, int bufferlen)
{
    QString content = "Not Defined";

    if(msgid == 0x4243)
    {
        switch(fcnid)
        {
        case 0:
        {
            const char temp[] = {0,0,0,0,0,0};
            qDebug() << "testpoint ";
            memcpy((void*)temp, (void*)(buffer+11), 4);
            qDebug() << "testpoint A";
            content = QString("Boot - content - ") + QString(temp);
            qDebug() << "testpoint B";
            qDebug() << "  message = " << content;
        }
            break;
        case 5:
        {
            QString motion = *(buffer + 11) == 0 ?"no flow":"flow";
            QString rotation = *(buffer + 12) == 0?"not rotating":"rotating";
            content = QString("Motion Status ") + motion + " " + rotation;
        }
            break;
        case 0x0a:
        {
            QString devicename = getDeviceDesc(*(buffer+11));
            QString verstring = QString("Firmware version ") + getDevVersion((unsigned char*)(buffer+12), 3);

            content = QString("dev:") + devicename + " " + verstring;
        }
            break;
        case 0x0b:
        {
            QString devicename = getDeviceDesc(*(buffer+11));
            QString verstring = QString("Hardware version ") + getDevVersion((unsigned char*)(buffer+12), 2);

            content = QString("dev:") + devicename + " " + verstring;
        }
            break;
        case 15:
        {
            QString devicename = getDeviceDesc(*(buffer+11));
            QString status = *(buffer+12) == 0 ? "false":"true";

            content = QString("dev:") + devicename + " initialized - status = " + status;
        }
            break;
        case 20:
        {
            char* temp = (char*)malloc(bufferlen);
            int msgsize = 9;

            memset(temp, 0, bufferlen);
            memcpy((void*)temp, (void*)(buffer+11), msgsize);
            content = QString("Start Mag Acq - content - ") + QString(temp);
            free(temp);
            break;
        }
        case 30:
        {
            char* temp = (char*)malloc(bufferlen);
            int msgsize = 10;

            memset(temp, 0, bufferlen);
            memcpy((void*)temp, (void*)(buffer+11), msgsize);
            content = QString("Start Gyro Acq - content - ") + QString(temp);
            free(temp);
            break;
        }
            break;
        case 49:
        {
            char* temp = (char*)malloc(bufferlen);
            int msgsize = bufferlen - 11 - 4;

            memset(temp, 0, bufferlen);
            memcpy((void*)temp, (void*)(buffer+11), msgsize);
            content = QString("Generic msg - content - ") + QString(temp);
            free(temp);
        }
            break;
        case 50:
            content = QString("Mag Survey Data - content - ");
            break;
        case 60:
            content = QString("Gyro Survey Data - content - ");
            break;
        default:
            break;
        }

    }

    return content;
}

QString HT_ToolParser::getDevVersion(unsigned char* buffer, int len)
{
    QString version = "v";

    for(int i=0;i<len;i++)
    {
        version += QString::number((unsigned int) buffer[i]);

        if(i<len-1)
        {
            version += ".";
        }
    }

    return version;
}

QString HT_ToolParser::getDeviceDesc(unsigned char type)
{
    QHash<unsigned char, QString> typemap;

    typemap.insert(10, "downhole controller");
    typemap.insert(30, "wireline transceiver");
    typemap.insert(40, "primary steering instrument");
    typemap.insert(70, "gyro survey instrument");
    typemap.insert(100, "secondary steering instrument");
    typemap.insert(130, "pressure module");

    return typemap.contains(type)?typemap.value(type):"undefined";
}

QHash<QString,QString> HT_ToolParser::parseRawMessage(char* buffer, int bufferlen)
{
    QHash<QString,QString> response;
    unsigned short msgid = swap16((unsigned char*)(buffer + RESPONSEID_OFFSET));
    unsigned short receivedcrc = swap16((unsigned char*)(buffer + (bufferlen-CRC_OFFSET)));;
    unsigned short calculatedcrc = ComputeCRC16((unsigned char*)buffer, bufferlen-CRC_OFFSET);
    unsigned long timestamp = swap32((unsigned char*)(buffer + TIMESTAMP_OFFSET));

    response.insert(RECEIVEDCRC, QString::number(receivedcrc));
    response.insert(CALCULATEDCRC, QString::number(calculatedcrc));
    response.insert(RESPONSETIMESTAMP, QString::number(timestamp));
    response.insert(RESPONSE_ID, QString::number(msgid));

    switch(msgid)
    {
    case 0x4243:    // broadcast
        response = parseBroadcastMessage(buffer, bufferlen, timestamp, receivedcrc, calculatedcrc);
        break;
    case 0x5152:    // query response
        response = parseQueryResponse(buffer, bufferlen);
        break;
    case 0x5352:    // set response
        response = parseSetResponse(buffer, bufferlen);
        break;
    case 0x4552:    // error response
        response = parseErrorResponse(buffer, bufferlen);
        break;
    default:
        break;
    }

    return response;
}

QHash<QString,QString> HT_ToolParser::parseBroadcastMessage(char* buffer, int bufferlen, unsigned long timestamp, unsigned short recvdcrc, unsigned short calculatedcrc)
{
    QHash<QString,QString> response;
    unsigned int fcnid = swap16((unsigned char*)(buffer + FUNCTIONID_OFFSET));

    response.insert(FUNCTION_ID, QString::number(fcnid));

    switch(fcnid)
    {
    case STARTUP:
    {
        QString devicename = getDeviceDesc(*(buffer + CONTENT_OFFSET));
        const char temp[] = {0,0,0,0,0,0};
        memcpy((void*)temp, (void*)(buffer + CONTENT_OFFSET + 2), 4);
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" dev:") + devicename + QString(" ") + QString(temp);
        response.insert(RESPONSE_CONTENT, content);
    }
        break;
    case MOTION_STATUS:
    {
        QString motion = *(buffer + CONTENT_OFFSET) == 0 ?"false":"true";
        QString rotation = *(buffer + CONTENT_OFFSET + 1) == 0?"false":"true";
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" Motion Status - flow = ") + motion + " rotation = " + rotation;

        response.insert(FLOW_STATUS, motion);
        response.insert(ROTATE_STATUS, rotation);
        response.insert(RESPONSE_CONTENT, content);
    }
        break;
    case FIRMWARE_VERSION:
    {
        QString devicename = getDeviceDesc(*(buffer + CONTENT_OFFSET));
        QString verstring = QString("Firmware version ") + getDevVersion((unsigned char*)(buffer+12), 4);
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" dev:") + devicename + " firmware version = " + verstring;

        response.insert(TOOL_DEVICE, devicename);
        response.insert(DEVICE_FIRMWARE_VERSION, verstring);
        response.insert(RESPONSE_CONTENT, content);
    }
        break;
    case HARDWARE_VERSION:
    {
        QString devicename = getDeviceDesc(*(buffer + CONTENT_OFFSET));
        QString verstring = QString("Hardware version ") + getDevVersion((unsigned char*)(buffer+12), 4);
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" dev:") + devicename + " hardware version = " + verstring;

        response.insert(TOOL_DEVICE, devicename);
        response.insert(DEVICE_HARDWARE_VERSION, verstring);
        response.insert(RESPONSE_CONTENT, content);
    }
        break;
    case MODULE_INITIALIZED:
    {
        QString devicename = getDeviceDesc(*(buffer + CONTENT_OFFSET));
        QString status = *(buffer + CONTENT_OFFSET + 1) == 0 ? "false":"true";
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" dev:") + devicename + " initialized - status = " + status;


        response.insert(TOOL_DEVICE, devicename);
        response.insert(DEVICE_INITIALIZED_STATUS, status);
        response.insert(RESPONSE_CONTENT, content);
    }
        break;
    case START_MAG_SURVEY:
    {
        char* temp = (char*)malloc(bufferlen);
        int msgsize = 9;

        msgsize = bufferlen - CONTENT_OFFSET - CRC_OFFSET;
        msgsize = *((unsigned char*)(buffer + CONTENT_OFFSET + 1)); // contains the message size

        memset(temp, 0, bufferlen);
        memcpy((void*)temp, (void*)(buffer + CONTENT_OFFSET + 2), msgsize);
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" Start Mag Acq - content - ") + QString(temp);
        free(temp);
        response.insert(RESPONSE_CONTENT, content);
    }
        break;
    case START_GRYO_SURVEY:
    {
        char* temp = (char*)malloc(bufferlen);
        int msgsize = 10;

        msgsize = bufferlen - CONTENT_OFFSET - CRC_OFFSET;
        msgsize = *((unsigned char*)(buffer + CONTENT_OFFSET + 1)); // contains the message size

        memset(temp, 0, bufferlen);
        memcpy((void*)temp, (void*)(buffer + CONTENT_OFFSET + 2), msgsize);
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" Start Gyro Acq - content - ") + QString(temp);
        free(temp);
        response.insert(RESPONSE_CONTENT, content);
    }
        break;
    case GENERIC_MESSAGE:
    {
        QString devicename = getDeviceDesc(*(buffer + CONTENT_OFFSET));
        char* temp = (char*)malloc(bufferlen);
        int msgsize = bufferlen - CONTENT_OFFSET - CRC_OFFSET - 1;

        msgsize = *((unsigned char*)(buffer + CONTENT_OFFSET + 1)); // contains the message size
        memset(temp, 0, bufferlen);
        memcpy((void*)temp, (void*)(buffer + CONTENT_OFFSET + 2), msgsize);
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" Generic msg for device:") + devicename + QString(" - content - ") + QString(temp);
        free(temp);
        response.insert(RESPONSE_CONTENT, content);
    }
        break;
    case MAG_SURVEY_DATA:
    {
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" Mag Survey Data - content - ");
        unsigned char* data = (unsigned char*) (buffer + CONTENT_OFFSET + 1);
        float inclination = HT_ToolParser::swapFloat(data);
        float azimuth = HT_ToolParser::swapFloat(data + 4);
        float mtf = HT_ToolParser::swapFloat(data + 8);
        float gtf = HT_ToolParser::swapFloat(data + 12);
        float totalmag = HT_ToolParser::swapFloat(data + 16);
        float totalg = HT_ToolParser::swapFloat(data + 20);

        content += " inc " + QString::number(inclination, 'f', 2);
        content += " azm " + QString::number(azimuth, 'f', 2);
        content += " mtf " + QString::number(mtf, 'f', 2);
        content += " gtf " + QString::number(gtf, 'f', 2);
        content += " totalM " + QString::number(totalmag, 'f', 6);
        content += " totalG " + QString::number(totalg, 'f', 6);

        response.insert(RESPONSE_CONTENT, content);
    }
        break;
    case MAG_SURVEY_FAILED:
    {
        char* temp = (char*)malloc(bufferlen);
        int msgsize = 9;

        msgsize = bufferlen - CONTENT_OFFSET - CRC_OFFSET;
        msgsize = *((unsigned char*)(buffer + CONTENT_OFFSET + 1)); // contains the message size

        memset(temp, 0, bufferlen);
        memcpy((void*)temp, (void*)(buffer+CONTENT_OFFSET + 2), msgsize);
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" Primary Mag Survey failed - content - ") + QString(temp);
        free(temp);
        response.insert(RESPONSE_CONTENT, content);
    }
        break;
    case START_CONTINUOUS_INC_AZM:
    {
        char* temp = (char*)malloc(bufferlen);
        int msgsize = 9;

        msgsize = bufferlen - CONTENT_OFFSET - CRC_OFFSET;
        msgsize = *((unsigned char*)(buffer + CONTENT_OFFSET) + 1); // contains the message size

        memset(temp, 0, bufferlen);
        memcpy((void*)temp, (void*)(buffer + CONTENT_OFFSET + 2), msgsize);
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" Start Continuous Inc&Azm - content - ") + QString(temp);
        free(temp);
        response.insert(RESPONSE_CONTENT, content);
    }
        break;
    case GYRO_SURVEY_DATA:
    {
        char* temp = (char*)malloc(bufferlen);
        int msgsize = bufferlen - CONTENT_OFFSET - CRC_OFFSET;
        msgsize = *((unsigned char*)(buffer + CONTENT_OFFSET)+1); // contains the message size

        memset(temp, 0, bufferlen);
        memcpy((void*)temp, (void*)(buffer + CONTENT_OFFSET + 2), msgsize);

        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" Gyro Survey Data - content - ") +  QString(temp);
        free(temp);


        msgsize = bufferlen - CONTENT_OFFSET - CRC_OFFSET;



        response.insert(RESPONSE_CONTENT, content);
    }
        break;

    default:
        QString header = QString(" timestamp:") + QString::number(timestamp) + QString(" rcvd CRC:0x") + QString::number(recvdcrc, 16) + QString(" calculated CRC:0x") + QString::number(calculatedcrc, 16);
        QString content = header + QString(" NOT HANDLED msg id = ") + QString::number(fcnid);
        response.insert(RESPONSE_CONTENT, content);
        break;
    }


    return response;
}

QHash<QString,QString> HT_ToolParser::parseQueryResponse(char* buffer, int bufferlen)
{
    QHash<QString,QString> response;

    return response;
}

QHash<QString,QString> HT_ToolParser::parseSetResponse(char* buffer, int bufferlen)
{
    QHash<QString,QString> response;

    return response;
}

QHash<QString,QString> HT_ToolParser::parseErrorResponse(char* buffer, int bufferlen)
{
    QHash<QString,QString> response;

    return response;
}

void MainWindow::testHTData()
{
        QList<QString> tooldata;

        tooldata << "02454d0000000042430000424f4f543a5b030402454d000000004243000a0a0001005b59030402454d000000004243000b0a001597ec030402454d000000004243000f0a015e8703040505";
        tooldata << "02454d000000014243000a1e0001060303fa030402454d000000014243000b1e138c4da3e037030402454d000000014243000f1e016bd303040505";
        tooldata << "02454d00000006424300312a5072696d617279205374656572696e6720546f6f6c204661696c656420746f20496e697469616c697a65a6d7030402454d000000064243000f2800a96c03040505";
        tooldata << "02454d00000010424300311e4779726f20546f6f6c204661696c656420746f20496e697469616c697a65c65d030402454d000000104243000f4600cb9103040505";
        tooldata << "02454d0000001a424300312c5365636f6e64617279205374656572696e6720546f6f6c204661696c656420746f20496e697469616c697a650d61030402454d0000001a4243000f640093bb03040505";

        for(int i=0;i<tooldata.size();i++)
        {
            char* buffer = convertHexString(tooldata.at(i));
            QStringList toolmessages = HT_ToolParser::parseBuffer(buffer, tooldata.at(i).size()/2);

            for(int j=0;j<toolmessages.size();j++)
            {
                qDebug() << "parsed this message " << toolmessages.at(j);
            }

            free(buffer);
        }

        QList<QByteArray> messages;
        unsigned int used = 0;

        for(int i=0;i<tooldata.size();i++)
        {
            char* buffer = convertHexString(tooldata.at(i));
            QList<QByteArray> bytes = HT_ToolParser::splitbuffers(buffer, tooldata.at(i).size()/2, &used);

            messages.append(bytes);
            free(buffer);
        }

        for(int i=0;i<messages.size();i++)
        {
            QByteArray msg = messages.at(i);
            QHash<QString,QString> attributes =  HT_ToolParser::parseRawMessage(msg.data(), msg.size());

            if(attributes.contains(RESPONSE_CONTENT))
            {
                QString content = attributes.value(RESPONSE_CONTENT);

                qDebug() << " msg content = " << content;
            }
            else
            {
                qDebug() << "!!!! no content found";
            }
        }

}
