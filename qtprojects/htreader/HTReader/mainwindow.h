#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/qserialport.h>
#include <qbytearray.h>
#include <QCloseEvent>

namespace Ui {
class MainWindow;
}


struct HT_Coefficients
{
    char functioncode;  // will be ascii '#'
    char SerialNum_hi;
    char SerialNum_lo;
    char filler1;
    char filler2;
    char filler3;
    char filler4;
    char HX_Bias_Factor_A0_hi;
    char HX_Bias_Factor_A0_lo;
    char HX_Bias_Factor_A1_hi;
    char HX_Bias_Factor_A1_lo;
    char HX_Bias_Factor_A2_hi;
    char HX_Bias_Factor_A2_lo;
    char HX_Bias_Factor_A3_hi;
    char HX_Bias_Factor_A3_lo;
    char HX_Scale_Factor_A0_hi;
    char HX_Scale_Factor_A0_lo;
    char HX_Scale_Factor_A1_hi;
    char HX_Scale_Factor_A1_lo;
    char HX_Scale_Factor_A2_hi;
    char HX_Scale_Factor_A2_lo;
    char HX_Scale_Factor_A3_hi;
    char HX_Scale_Factor_A3_lo;
    char HX_Cross_Axis_Factor_K1_hi;
    char HX_Cross_Axis_Factor_K1_lo;
    char HX_Cross_Axis_Factor_K2_hi;
    char HX_Cross_Axis_Factor_K2_lo;
    char HX_CheckSum;
    char HY_Bias_Factor_A0_hi;
    char HY_Bias_Factor_A0_lo;
    char HY_Bias_Factor_A1_hi;
    char HY_Bias_Factor_A1_lo;
    char HY_Bias_Factor_A2_hi;
    char HY_Bias_Factor_A2_lo;
    char HY_Bias_Factor_A3_hi;
    char HY_Bias_Factor_A3_lo;
    char HY_Scale_Factor_A0_hi;
    char HY_Scale_Factor_A0_lo;
    char HY_Scale_Factor_A1_hi;
    char HY_Scale_Factor_A1_lo;
    char HY_Scale_Factor_A2_hi;
    char HY_Scale_Factor_A2_lo;
    char HY_Scale_Factor_A3_hi;
    char HY_Scale_Factor_A3_lo;
    char HY_Cross_Axis_Factor_K1_hi;
    char HY_Cross_Axis_Factor_K1_lo;
    char HY_Cross_Axis_Factor_K2_hi;
    char HY_Cross_Axis_Factor_K2_lo;
    char HY_CheckSum;   // applies to to the previous 20 values
    char HZ_Bias_Factor_A0_hi;
    char HZ_Bias_Factor_A0_lo;
    char HZ_Bias_Factor_A1_hi;
    char HZ_Bias_Factor_A1_lo;
    char HZ_Bias_Factor_A2_hi;
    char HZ_Bias_Factor_A2_lo;
    char HZ_Bias_Factor_A3_hi;
    char HZ_Bias_Factor_A3_lo;
    char HZ_Scale_Factor_A0_hi;
    char HZ_Scale_Factor_A0_lo;
    char HZ_Scale_Factor_A1_hi;
    char HZ_Scale_Factor_A1_lo;
    char HZ_Scale_Factor_A2_hi;
    char HZ_Scale_Factor_A2_lo;
    char HZ_Scale_Factor_A3_hi;
    char HZ_Scale_Factor_A3_lo;
    char HZ_Cross_Axis_Factor_K1_hi;
    char HZ_Cross_Axis_Factor_K1_lo;
    char HZ_Cross_Axis_Factor_K2_hi;
    char HZ_Cross_Axis_Factor_K2_lo;
    char HZ_CheckSum;   // applies to to the previous 20 values
    char GX_Bias_Factor_A0_hi;
    char GX_Bias_Factor_A0_lo;
    char GX_Bias_Factor_A1_hi;
    char GX_Bias_Factor_A1_lo;
    char GX_Bias_Factor_A2_hi;
    char GX_Bias_Factor_A2_lo;
    char GX_Bias_Factor_A3_hi;
    char GX_Bias_Factor_A3_lo;
    char GX_Scale_Factor_A0_hi;
    char GX_Scale_Factor_A0_lo;
    char GX_Scale_Factor_A1_hi;
    char GX_Scale_Factor_A1_lo;
    char GX_Scale_Factor_A2_hi;
    char GX_Scale_Factor_A2_lo;
    char GX_Scale_Factor_A3_hi;
    char GX_Scale_Factor_A3_lo;
    char GX_Cross_Axis_Factor_K1_hi;
    char GX_Cross_Axis_Factor_K1_lo;
    char GX_Cross_Axis_Factor_K2_hi;
    char GX_Cross_Axis_Factor_K2_lo;
    char GX_CheckSum;   // applies to to the previous 20 values
    char GY_Bias_Factor_A0_hi;
    char GY_Bias_Factor_A0_lo;
    char GY_Bias_Factor_A1_hi;
    char GY_Bias_Factor_A1_lo;
    char GY_Bias_Factor_A2_hi;
    char GY_Bias_Factor_A2_lo;
    char GY_Bias_Factor_A3_hi;
    char GY_Bias_Factor_A3_lo;
    char GY_Scale_Factor_A0_hi;
    char GY_Scale_Factor_A0_lo;
    char GY_Scale_Factor_A1_hi;
    char GY_Scale_Factor_A1_lo;
    char GY_Scale_Factor_A2_hi;
    char GY_Scale_Factor_A2_lo;
    char GY_Scale_Factor_A3_hi;
    char GY_Scale_Factor_A3_lo;
    char GY_Cross_Axis_Factor_K1_hi;
    char GY_Cross_Axis_Factor_K1_lo;
    char GY_Cross_Axis_Factor_K2_hi;
    char GY_Cross_Axis_Factor_K2_lo;
    char GY_CheckSum;   // applies to to the previous 20 values
    char GZ_Bias_Factor_A0_hi;
    char GZ_Bias_Factor_A0_lo;
    char GZ_Bias_Factor_A1_hi;
    char GZ_Bias_Factor_A1_lo;
    char GZ_Bias_Factor_A2_hi;
    char GZ_Bias_Factor_A2_lo;
    char GZ_Bias_Factor_A3_hi;
    char GZ_Bias_Factor_A3_lo;
    char GZ_Scale_Factor_A0_hi;
    char GZ_Scale_Factor_A0_lo;
    char GZ_Scale_Factor_A1_hi;
    char GZ_Scale_Factor_A1_lo;
    char GZ_Scale_Factor_A2_hi;
    char GZ_Scale_Factor_A2_lo;
    char GZ_Scale_Factor_A3_hi;
    char GZ_Scale_Factor_A3_lo;
    char GZ_Cross_Axis_Factor_K1_hi;
    char GZ_Cross_Axis_Factor_K1_lo;
    char GZ_Cross_Axis_Factor_K2_hi;
    char GZ_Cross_Axis_Factor_K2_lo;
    char GZ_CheckSum;   // applies to to the previous 20 values
};

struct HT_RawAxisData
{
    char functioncode;  // will be ascii 'Y'
    char HX_Raw_hibyte;
    char HX_Raw_middlebyte;
    char HX_Raw_lobyte;
    char HY_Raw_hibyte;
    char HY_Raw_middlebyte;
    char HY_Raw_lobyte;
    char HZ_Raw_hibyte;
    char HZ_Raw_middlebyte;
    char HZ_Raw_lobyte;
    char GX_Raw_hibyte;
    char GX_Raw_middlebyte;
    char GX_Raw_lobyte;
    char GY_Raw_hibyte;
    char GY_Raw_middlebyte;
    char GY_Raw_lobyte;
    char GZ_Raw_hibyte;
    char GZ_Raw_middlebyte;
    char GZ_Raw_lobyte;
    char Temp_hibyte;
    char Temp_middlebyte;
    char Temp_lobyte;
    char Voltage_hibyte;
    char Voltage_middlebyte;
    char Voltage_lobyte;
    char CheckSum;
} ;

class HTData
{
public:
    static double convertRawAxis(char hibyte, char middlebyte, char lobyte);
    static double convertCoeffecient(char hibyte, char lobyte);
    static long calcRecordCheckSum(char* pstart, int count, long checksumoffset);
    static long calcCheckSumOffset(char b1, char b2, char b3);

    static double convertRawTemp(double temperature);
    static double convertRawVoltage(double voltage);

    static double calcBiasFactor(double biasfactorA0, double biasfactorA1, double biasfactorA2, double biasfactorA3, double tempC);
    static double calcScaleFactor(double scalefactorA0, double scalefactorA1, double scalefactorA2, double scalefactorA3, double tempC);

    static double adjustRawVector(double rawvalue, double scalefactor, double bias,
                                  double crossfactor1, double raw1, double scale1, double bias1,
                                  double crossfactor2, double raw2, double scale2, double bias2);


    static void parseBuffer(char* buffer, int bufferlen);
    static void parseCoeffecientsBuffer(char* buffer, int bufferlen);
    static void parseRawBuffer(char* buffer, int bufferlen);
    static QString getMessageContent(unsigned int msgid, unsigned int fcnid, char* buffer);
};

#define TIMESTAMP_OFFSET 3
#define RESPONSEID_OFFSET 7
#define FUNCTIONID_OFFSET 9
#define CONTENT_OFFSET 11
#define CRC_OFFSET 4

#define RECORD_BEGIN 0x02454d
#define RECORD_END 0x0304

#define DOWNHOLE_CONTROLLER 10
#define WIRELINE_TRANSCEIVER 30
#define PRIMARY_STEERING_INSTRUMENT 40
#define SECONDARY_STEERING_INSTRUMENT 100
#define GYRO_SURVEY_INSTRUMENT 70
#define PRESSURE_MODULE 130


#define STARTUP 0
#define MOTION_STATUS 5
#define FIRMWARE_VERSION 10
#define HARDWARE_VERSION 11
#define MODULE_INITIALIZED 15
#define GENERIC_MESSAGE 19
#define START_MAG_SURVEY 40
#define MAG_SURVEY_DATA 41
#define MAG_SURVEY_FAILED 42
#define START_CONTINUOUS_INC_AZM 44
#define START_GRYO_SURVEY 70
#define GYRO_SURVEY_DATA 71
#define TEMPERATURE_DATA 100        // !!!! this is not correct...


#define RECEIVEDCRC "receivedcrc"
#define CALCULATEDCRC "calculatedcrc"
#define RESPONSETIMESTAMP "responsetimestamp"

#define RESPONSE_ID "responseid"
#define FUNCTION_ID "functionid"
#define RESPONSE_CONTENT "responsecontent"
#define FLOW_STATUS "flowstatus"
#define ROTATE_STATUS "rotationstatus"
#define TOOL_DEVICE "devicetype"
#define DEVICE_FIRMWARE_VERSION "firmwareversion"
#define DEVICE_HARDWARE_VERSION "hardwareversion"
#define DEVICE_INITIALIZED_STATUS "deviceinitialized"


class HT_ToolParser
{
public:
    static QStringList parseBuffer(char* buffer, int bufferlen);
    static QList<QByteArray> splitbuffers(char* buffer, int bufferlen, unsigned int *consumedcount);
    static unsigned short ComputeCRC16(unsigned char* buf, unsigned len);
    static unsigned short calcCRC(QByteArray buffer);
    static QString getDescripton(QByteArray buffer);
    static QString getMessageContent(unsigned int msgid, unsigned int fcnid, char* buffer, int bufferlen);
    static QString getDeviceDesc(unsigned char type);
    static QString getDevVersion(unsigned char* buffer, int len);

    static unsigned int swap16(unsigned char* buffer);
    static unsigned long swap32(unsigned char* buffer);
    static unsigned long swap24(unsigned char* buffer);
    static float swapFloat(unsigned char* buffer);

    static QHash<QString,QString> parseRawMessage(char* buffer, int bufferlen);
    static QHash<QString,QString> parseBroadcastMessage(char* buffer, int bufferlen, unsigned long timestamp, unsigned short recvdcrc, unsigned short calculatedcrc);
    static QHash<QString,QString> parseQueryResponse(char* buffer, int bufferlen);
    static QHash<QString,QString> parseSetResponse(char* buffer, int bufferlen);
    static QHash<QString,QString> parseErrorResponse(char* buffer, int bufferlen);

};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    char* convertHexString(QString str);

    void testHTData();

    void testFloatConversion();

protected:
    void closeEvent(QCloseEvent *event);

public slots:
    void dataAvailable();

private:
    Ui::MainWindow *ui;
    QSerialPort* m_pSerialPort;
    QByteArray m_bytes;
    long long m_lastReadTime;

    QList<QByteArray> m_receivedData;
};

#endif // MAINWINDOW_H
