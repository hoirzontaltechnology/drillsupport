#-------------------------------------------------
#
# Project created by QtCreator 2017-06-13T14:03:25
#
#-------------------------------------------------

QT  += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HTReader
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
