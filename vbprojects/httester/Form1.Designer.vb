﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.incdisplay = New System.Windows.Forms.TextBox()
        Me.azmdisplay = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dipdisplay = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.mtfadisplay = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cinclination = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cazimuth = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.annularpressure = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.drillpressure = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.toolserialdisplay = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.voltagedisplay = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.temperaturedisplay = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.datedisplay = New System.Windows.Forms.Label()
        Me.commstats = New System.Windows.Forms.Label()
        Me.flowstate = New System.Windows.Forms.Label()
        Me.rotationstate = New System.Windows.Forms.Label()
        Me.toolrpm = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(12, 12)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox1.Size = New System.Drawing.Size(437, 135)
        Me.TextBox1.TabIndex = 0
        '
        'SerialPort1
        '
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 216)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(22, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Inc"
        '
        'incdisplay
        '
        Me.incdisplay.Location = New System.Drawing.Point(60, 216)
        Me.incdisplay.Name = "incdisplay"
        Me.incdisplay.Size = New System.Drawing.Size(100, 20)
        Me.incdisplay.TabIndex = 2
        '
        'azmdisplay
        '
        Me.azmdisplay.Location = New System.Drawing.Point(60, 242)
        Me.azmdisplay.Name = "azmdisplay"
        Me.azmdisplay.Size = New System.Drawing.Size(100, 20)
        Me.azmdisplay.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 242)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(27, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Azm"
        '
        'dipdisplay
        '
        Me.dipdisplay.Location = New System.Drawing.Point(60, 268)
        Me.dipdisplay.Name = "dipdisplay"
        Me.dipdisplay.Size = New System.Drawing.Size(100, 20)
        Me.dipdisplay.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(14, 268)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Dip A"
        '
        'mtfadisplay
        '
        Me.mtfadisplay.Location = New System.Drawing.Point(60, 294)
        Me.mtfadisplay.Name = "mtfadisplay"
        Me.mtfadisplay.Size = New System.Drawing.Size(100, 20)
        Me.mtfadisplay.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 294)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "MTFA"
        '
        'cinclination
        '
        Me.cinclination.Location = New System.Drawing.Point(227, 216)
        Me.cinclination.Name = "cinclination"
        Me.cinclination.Size = New System.Drawing.Size(100, 20)
        Me.cinclination.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(181, 216)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "CInc"
        '
        'cazimuth
        '
        Me.cazimuth.Location = New System.Drawing.Point(227, 242)
        Me.cazimuth.Name = "cazimuth"
        Me.cazimuth.Size = New System.Drawing.Size(100, 20)
        Me.cazimuth.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(181, 242)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(34, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "CAzm"
        '
        'annularpressure
        '
        Me.annularpressure.Location = New System.Drawing.Point(259, 268)
        Me.annularpressure.Name = "annularpressure"
        Me.annularpressure.Size = New System.Drawing.Size(100, 20)
        Me.annularpressure.TabIndex = 14
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(181, 268)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Annular Press"
        '
        'drillpressure
        '
        Me.drillpressure.Location = New System.Drawing.Point(259, 291)
        Me.drillpressure.Name = "drillpressure"
        Me.drillpressure.Size = New System.Drawing.Size(100, 20)
        Me.drillpressure.TabIndex = 16
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(181, 294)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(53, 13)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "Drill Press"
        '
        'toolserialdisplay
        '
        Me.toolserialdisplay.Location = New System.Drawing.Point(457, 268)
        Me.toolserialdisplay.Name = "toolserialdisplay"
        Me.toolserialdisplay.Size = New System.Drawing.Size(100, 20)
        Me.toolserialdisplay.TabIndex = 22
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(379, 268)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(57, 13)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "Tool Serial"
        '
        'voltagedisplay
        '
        Me.voltagedisplay.Location = New System.Drawing.Point(425, 242)
        Me.voltagedisplay.Name = "voltagedisplay"
        Me.voltagedisplay.Size = New System.Drawing.Size(100, 20)
        Me.voltagedisplay.TabIndex = 20
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(379, 242)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 13)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Voltage"
        '
        'temperaturedisplay
        '
        Me.temperaturedisplay.Location = New System.Drawing.Point(425, 216)
        Me.temperaturedisplay.Name = "temperaturedisplay"
        Me.temperaturedisplay.Size = New System.Drawing.Size(100, 20)
        Me.temperaturedisplay.TabIndex = 18
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(379, 216)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(34, 13)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "Temp"
        '
        'datedisplay
        '
        Me.datedisplay.AutoSize = True
        Me.datedisplay.Location = New System.Drawing.Point(14, 362)
        Me.datedisplay.Name = "datedisplay"
        Me.datedisplay.Size = New System.Drawing.Size(111, 13)
        Me.datedisplay.TabIndex = 23
        Me.datedisplay.Text = "this is the current date"
        '
        'commstats
        '
        Me.commstats.AutoSize = True
        Me.commstats.Location = New System.Drawing.Point(14, 333)
        Me.commstats.Name = "commstats"
        Me.commstats.Size = New System.Drawing.Size(150, 13)
        Me.commstats.TabIndex = 24
        Me.commstats.Text = "comm stats  bad/total packets"
        '
        'flowstate
        '
        Me.flowstate.AutoSize = True
        Me.flowstate.Location = New System.Drawing.Point(209, 333)
        Me.flowstate.Name = "flowstate"
        Me.flowstate.Size = New System.Drawing.Size(52, 13)
        Me.flowstate.TabIndex = 25
        Me.flowstate.Text = "flow state"
        '
        'rotationstate
        '
        Me.rotationstate.AutoSize = True
        Me.rotationstate.Location = New System.Drawing.Point(209, 362)
        Me.rotationstate.Name = "rotationstate"
        Me.rotationstate.Size = New System.Drawing.Size(68, 13)
        Me.rotationstate.TabIndex = 26
        Me.rotationstate.Text = "rotation state"
        '
        'toolrpm
        '
        Me.toolrpm.Location = New System.Drawing.Point(434, 294)
        Me.toolrpm.Name = "toolrpm"
        Me.toolrpm.Size = New System.Drawing.Size(100, 20)
        Me.toolrpm.TabIndex = 28
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(388, 294)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(31, 13)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "RPM"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(591, 387)
        Me.Controls.Add(Me.toolrpm)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.rotationstate)
        Me.Controls.Add(Me.flowstate)
        Me.Controls.Add(Me.commstats)
        Me.Controls.Add(Me.datedisplay)
        Me.Controls.Add(Me.toolserialdisplay)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.voltagedisplay)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.temperaturedisplay)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.drillpressure)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.annularpressure)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cazimuth)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cinclination)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.mtfadisplay)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dipdisplay)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.azmdisplay)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.incdisplay)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents incdisplay As System.Windows.Forms.TextBox
    Friend WithEvents azmdisplay As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dipdisplay As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents mtfadisplay As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cinclination As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cazimuth As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents annularpressure As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents drillpressure As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents toolserialdisplay As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents voltagedisplay As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents temperaturedisplay As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents datedisplay As System.Windows.Forms.Label
    Friend WithEvents commstats As System.Windows.Forms.Label
    Friend WithEvents flowstate As System.Windows.Forms.Label
    Friend WithEvents rotationstate As System.Windows.Forms.Label
    Friend WithEvents toolrpm As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label

End Class
