﻿Imports System
Imports System.IO


Public Class HTToolParser
    Public Const RESPONSE_ID As String = "responseid"
    Public Const FUNCTION_ID As String = "functionid"
    Public Const RESPONSE_CONTENT As String = "responsecontent"
    Public Const FLOW_STATUS As String = "flowstatus"
    Public Const ROTATE_STATUS As String = "rotationstatus"
    Public Const TOOL_DEVICE As String = "devicetype"
    Public Const TOOL_DEVICE_DESCRIPTION As String = "devicedescription"
    Public Const DEVICE_FIRMWARE_VERSION As String = "firmwareversion"
    Public Const DEVICE_HARDWARE_VERSION As String = "hardwareversion"
    Public Const DEVICE_INITIALIZED_STATUS As String = "deviceinitialized"
    Public Const RECEIVED_CRC As String = "receivedcrc"
    Public Const CALCULATED_CRC As String = "calculatedcrc"
    Public Const TIMESTAMP As String = "timestamp"
    Public Const DEVICE_GENERIC_MSG As String = "genericmessage"

    Public Const INCLINATION_T As String = "inclination"
    Public Const AZIMUTH_T As String = "azimuth"
    Public Const DIP_ANGLE As String = "dipangle"
    Public Const MAG_TOOLFACE As String = "magtoolface"
    Public Const GRAV_TOOLFACE As String = "gravtoolface"
    Public Const TOTAL_MAGF As String = "totalmag"
    Public Const TOTAL_GRAVF As String = "totalgrav"
    Public Const AX_VECTOR As String = "ax"
    Public Const AY_VECTOR As String = "ay"
    Public Const AZ_VECTOR As String = "az"
    Public Const MX_VECTOR As String = "mx"
    Public Const MY_VECTOR As String = "my"
    Public Const MZ_VECTOR As String = "mz"

    Public Const DRILL_PRESSURE As String = "drillpressure"
    Public Const ANNULAR_PRESSURE As String = "annularpressure"

    Public Const CINCLINATION As String = "cinc"
    Public Const CAZMIMUTH As String = "cazm"

    Public Const WIRELINE_VOLTS As String = "voltage"
    Public Const DRILL_TEMPERATUE As String = "drilltemp"
    Public Const ANNULAR_TEMPERATURE As String = "annulartemp"
    Public Const TOOL_SERIAL_NUMBER As String = "toolserialnum"

    Public Const AXIAL_VIBRATION As String = "axialvibe"
    Public Const RADIAL_VIBRATION As String = "radialvibe"
    Public Const AXIAL_SHOCK As String = "axialshock"
    Public Const RADIAL_SHOCK As String = "radialshock"
    Public Const AXIAL_VIBRATION_LEVEL As String = "axialvibelevel"
    Public Const RADIAL_VIBRATION_LEVEL As String = "radialvibelevel"
    Public Const AXIAL_SHOCK_LEVEL As String = "axialshocklevel"
    Public Const RADIAL_SHOCK_LEVEL As String = "radialshocklevel"
    Public Const AXIAL_SHOCK_COUNT As String = "axialshockcount"
    Public Const RADIAL_SHOCK_COUNT As String = "radialshockcount"
    Public Const AXIAL_SHOCK_MAX As String = "axailshockmax"
    Public Const RADIAL_SHOCK_MAX As String = "radialshockmax"
    Public Const TOOL_RPM As String = "toolrpm"

    Public Const PARSE_ERROR As String = "parsingerror"
    Public Const UNIX_TIME As String = "Time(unix)"


    Public Const TOOLHEADER As Integer = &H2454D
    Public Const TOOLFOOTER As Integer = &H304
    Public Const BROADCAST_ID As Integer = &H4243

    Public Const MESSAGE_ID_OFFSET As Integer = 8
    Public Const TIMESTAMP_OFFSET As Integer = 4
    Public Const BROADCAST_FCNID_OFFSET As Integer = 10
    Public Const BROADCAST_DEVICENUM_OFFSET As Integer = 12
    Public Const BROADCAST_CONTENT_OFFSET As Integer = 13

    Public Const STARTUP As Integer = 0
    Public Const MOTION_STATUS As Integer = 5
    Public Const FIRMWARE_VERSION As Integer = 10
    Public Const HARDWARE_VERSION As Integer = 11
    Public Const SERIAL_NUMBER As Integer = 12
    Public Const MODULE_INITIALIZED As Integer = 15
    Public Const WIRELINE_VOLTAGE As Integer = 17
    Public Const GENERIC_MESSAGE As Integer = 19
    Public Const START_MAG_SURVEY As Integer = 40
    Public Const MAG_SURVEY_DATA As Integer = 41
    Public Const MAG_SURVEY_FAILED As Integer = 42
    Public Const START_CONTINUOUS_INC_AZM As Integer = 44
    Public Const CONTINUOUS_SURVEY_DATA As Integer = 45
    Public Const SHOCK_AND_VIB_DATA_STARTED As Integer = 50
    Public Const SHOCK_AND_VIB_DATA As Integer = 51
    Public Const SHOCK_AND_VIB_FAILED As Integer = 52
    Public Const START_GRYO_SURVEY As Integer = 70
    Public Const GYRO_SURVEY_DATA As Integer = 71
    Public Const GYRO_SURVEY_FAILED As Integer = 72
    Public Const START_SECONDARY_MAG_SURVEY As Integer = 100
    Public Const SECONDARY_MAG_SURVEY_DATA As Integer = 101

    Public Const START_PRESSURE_ACQUISITION As Integer = 130
    Public Const PRESSURE_DATA As Integer = 131
    Public Const TEMPERATURE_DATA As Integer = 103  ' technically, this not defined yet...

    Public Delegate Function HANDLEToolData(ByVal inclination As Single, ByVal azimuth As Single, ByVal magtoolface As Single,
                                         ByVal gravtoolface As Single, ByVal magtotal As Single, ByVal gravtotal As Single, ByVal dipangle As Single,
                                         ByVal ax As Single, ByVal ay As Single, ByVal az As Single, ByVal mx As Single, ByVal my As Single, ByVal mz As Single,
                                         ByVal annularpressure As Single, ByVal drillpressure As Single, ByVal voltage As Single, ByVal temperatrue As Single,
                                         ByVal serial As String, ByVal crcerror As Boolean, ByVal timestamp As Date)
    Public Delegate Function HANDLEDirectionalData(ByVal inclination As Single, ByVal azimuth As Single, ByVal magtoolface As Single,
                                         ByVal gravtoolface As Single, ByVal magtotal As Single, ByVal gravtotal As Single, ByVal dipangle As Single, ByVal timestamp As Date)
    Public Delegate Function HANDLESixAxisData(ByVal ax As Single, ByVal ay As Single, ByVal az As Single,
                                     ByVal mx As Single, ByVal my As Single, ByVal mz As Single, ByVal timestamp As Date)
    Public Delegate Function HANDLEPressureData(ByVal annularpressure As Single, ByVal drillpressure As Single, ByVal timestamp As Date)
    Public Delegate Function HANDLEContinuousSurveyData(ByVal cinc As Single, ByVal cazm As Single, ByVal timestamp As Date)
    Public Delegate Function HANDLEShockAndVibData()
    Public Delegate Function HANDLEFWVersion(ByVal version As String, ByVal tool As String)
    Public Delegate Function HANDLEHWVersion(ByVal version As String, ByVal tool As String)
    Public Delegate Function HANDLEBoot()
    Public Delegate Function HANDLEMessage(ByVal msg As String, ByVal tool As String)
    Public Delegate Function HANDLESerialNumber(ByVal serial As String, ByVal tool As String)
    Public Delegate Function HANDLEMotionStatus(ByVal flow As Boolean, ByVal rotation As Boolean)
    Public Delegate Function HANDLEGyroSurvey()
    Public Delegate Function HANDLEWirelineVoltage(ByVal voltage As Single, ByVal timestamp As Date)
    Public Delegate Function HANDLETemperature(ByVal drilltemp As Single, ByVal annulartemp As Single, ByVal timestamp As Date)
    Public Delegate Function HANDLECommStats(ByVal goodcrccount As Integer, ByVal failedcrccount As Integer, ByVal generalfailure As Integer, ByVal notraffic As Boolean, ByVal nomessages As Boolean)
    Public Delegate Function HANDLEShockAndVibeData(axialshocklevel As Integer, radialshocklevel As Integer,
                                                    axialviblevel As Integer, radialviblevel As Integer,
                                                    axialviberms As Single, radialviberms As Single,
                                                    axialshockcount As Integer, radialshockcount As Integer,
                                                    axialshockmax As Single, radialshockmax As Single,
                                                    toolrpm As Single)
    Public Delegate Function HANDLEMotionStatusData(flow As Boolean, rotation As Boolean)
    Public Delegate Function HANDLEToolDataHashMap(ByRef tooldata As ArrayList, ByRef datamap As Hashtable)

    Public toolDataHandler As HANDLEToolData = Nothing
    Public directionalHandler As HANDLEDirectionalData = Nothing
    Public sixAxisHandler As HANDLESixAxisData = Nothing
    Public pressureHandler As HANDLEPressureData = Nothing
    Public continuousSurveyHandler As HANDLEContinuousSurveyData = Nothing
    Public bootHandler As HANDLEBoot = Nothing
    Public motionStatusHandler As HANDLEMotionStatus = Nothing
    Public messageHandler As HANDLEMessage = Nothing
    Public wirelineVoltageHandler As HANDLEWirelineVoltage = Nothing
    Public temperatureHandler As HANDLETemperature = Nothing
    Public comStatsHandler As HANDLECommStats = Nothing
    Public shockAndVibeHandler As HANDLEShockAndVibeData = Nothing
    Public motionStatusDataHandler As HANDLEMotionStatusData = Nothing
    Public toolDataHashMapHandler As HANDLEToolDataHashMap = Nothing
    Public serialNumberHandler As HANDLESerialNumber = Nothing
    Public fwVersionHandler As HANDLEFWVersion = Nothing
    Public hwVersionHandler As HANDLEHWVersion = Nothing


    Private annularPressure As Single = 0
    Private drillPressure As Single = 0
    Private annularTemperature As Single = 0
    Private drillTemperature As Single = 0
    Private wirelineVoltage As Single = 0
    Private toolSerialNumber As String = "---"

    Private rawToolData As New ArrayList    '' cache for received data that is incomplete
    Private timeoffset As Date              '' stores the time of the boot message - used as an offset for tool time
    Private crcFailedCount As Integer = 0
    Private crcPassedCount As Integer = 0
    Private generalFailures As Integer = 0
    Private noTrafficCondition As Boolean = True
    Private noMessagesCondition As Boolean = True
    Private trafficTimer As New System.Timers.Timer

    Private toolHeaderValues As String() = {UNIX_TIME, TIMESTAMP, RESPONSE_CONTENT, RESPONSE_ID, FUNCTION_ID, TOOL_DEVICE, TOOL_DEVICE_DESCRIPTION, FLOW_STATUS, ROTATE_STATUS, DEVICE_FIRMWARE_VERSION, DEVICE_HARDWARE_VERSION, DEVICE_INITIALIZED_STATUS,
RECEIVED_CRC, CALCULATED_CRC, DEVICE_GENERIC_MSG, INCLINATION_T, AZIMUTH_T, DIP_ANGLE, MAG_TOOLFACE, GRAV_TOOLFACE, TOTAL_MAGF, TOTAL_GRAVF, AX_VECTOR,
AY_VECTOR, AZ_VECTOR, MX_VECTOR, MY_VECTOR, MZ_VECTOR, DRILL_PRESSURE, ANNULAR_PRESSURE, CINCLINATION, CAZMIMUTH, WIRELINE_VOLTS, DRILL_TEMPERATUE, ANNULAR_TEMPERATURE,
TOOL_SERIAL_NUMBER, AXIAL_VIBRATION, RADIAL_VIBRATION, AXIAL_VIBRATION_LEVEL, RADIAL_VIBRATION_LEVEL, AXIAL_SHOCK_LEVEL, RADIAL_SHOCK_LEVEL,
AXIAL_SHOCK_COUNT, RADIAL_SHOCK_COUNT, AXIAL_SHOCK_MAX, RADIAL_SHOCK_MAX, TOOL_RPM, PARSE_ERROR}

    Private toolDataHeaders As New ArrayList
    Private headerWritten As Boolean = False
    Private toolDataFileName As String = "tooldata.csv"
    Private toolDataFile As StreamWriter = Nothing
    Private shouldLogToolData As Boolean = True

    Public Function setLogToolData(ByVal logtooldata As Boolean)
        shouldLogToolData = logtooldata
        Return Nothing
    End Function

    Public Function resetState()

        crcFailedCount = 0
        crcPassedCount = 0
        generalFailures = 0
        rawToolData.Clear()
        trafficTimer.Interval = 2000
        trafficTimer.Enabled = True
        RemoveHandler trafficTimer.Elapsed, AddressOf OnTrafficCheckTimer
        AddHandler trafficTimer.Elapsed, AddressOf OnTrafficCheckTimer
        noTrafficCondition = True
        noMessagesCondition = True

        Return Nothing
    End Function

    Public Function parseAndHandleToolData(ByRef buffer() As Byte)

        Dim messages As ArrayList = parseToolData(buffer)

        For Each msg As ArrayList In messages

            Dim map As Hashtable = parseRawToolData(msg)

            If toolDataHashMapHandler IsNot Nothing Then
                toolDataHashMapHandler(msg, map)
            End If

        Next

        Return Nothing
    End Function

    '' take the incoming data and split it into separate buffers each of which is
    '' a single message from tool
    Public Function parseToolData(ByRef Buffer() As Byte) As ArrayList
        Dim list As New ArrayList
        Dim currentmsg As New ArrayList
        Dim bufferlen As Integer = Buffer.GetUpperBound(0)
        Dim currentmsglen As Integer = -1
        Dim currentbufferposition As Integer = 0
        Dim completebufferfound As Boolean = False

        noTrafficCondition = False
        '' copy the input into a cache
        For I As Integer = 0 To bufferlen
            rawToolData.Add(Buffer(I))
        Next

        'Debug.WriteLine("got this many bytes " + bufferlen.ToString() + " msg = " + getStringRep(rawToolData))

        bufferlen = rawToolData.Count

        For I As Integer = 0 To bufferlen - 1
            Dim skiptherest As Boolean = False

            '' do we have a tool header???
            If (I + 2) < bufferlen AndAlso extractU24(rawToolData, I) = TOOLHEADER Then
                skiptherest = True  '' indicates not to add these bytes - they are added here

                'Debug.WriteLine("got header")

                currentmsg.Clear()  '' anything stored up to this time is noise...

                For j As Integer = 0 To 2
                    currentmsg.Add(rawToolData(I + j))
                Next

                I += 2

                If I + 1 < bufferlen Then
                    Dim messagelen As Integer = rawToolData(I + 1) + 8      '' add 4 bytes for the header and len and 4 bytes for CRC and footer

                    'Debug.WriteLine("got header with message of this length " + messagelen.ToString() + " index = " + I.ToString)
                    currentmsglen = messagelen
                End If
            End If

            If Not skiptherest Then

                currentmsg.Add(rawToolData(I))

                If currentmsglen <> -1 AndAlso currentmsg.Count = currentmsglen Then
                    list.Add(currentmsg)
                    currentmsg = New ArrayList  '' found the footer start a new message
                    currentmsglen = -1
                    currentbufferposition = I   '' mark the end of the last complete record...
                    completebufferfound = True
                End If
            End If
        Next

        '' dump the used bytes - leave the rest for later
        If rawToolData.Count > 0 And completebufferfound Then
            rawToolData.RemoveRange(0, currentbufferposition + 1)
        End If

        Return list
    End Function

    Public Function getStringRep(ByVal array As ArrayList) As String
        Dim rep As String = ""

        For Each val As Byte In array
            rep += val.ToString("x2")
        Next

        Return rep
    End Function

    Public Function calcCRC16(ByVal tooldata As ArrayList, ByVal len As Integer) As UInt16
        Dim crc As UInt16 = 0

        For i As Integer = 0 To len - 1
            Dim b As Byte = tooldata(i)

            For j As Integer = 0 To 7
                crc = If(((b Xor crc) And 1), ((crc >> 1) Xor &HA001), (crc >> 1))
                b >>= 1
            Next
        Next

        Return crc
    End Function

    Public Function parseRawToolData(ByVal tooldata As ArrayList) As Hashtable
        Dim map As New Hashtable
        Dim msgid As UInt16 = extractU16(tooldata, MESSAGE_ID_OFFSET)
        Dim tStamp As UInt32 = extractU32(tooldata, TIMESTAMP_OFFSET)
        Dim receivedcrc As UInt16 = extractU16(tooldata, tooldata.Count - 4)
        Dim calculatedcrc As UInt16 = calcCRC16(tooldata, tooldata.Count - 4)
        Dim crcpassed As Boolean = (receivedcrc = calculatedcrc)
        Dim unixtime As Int64 = (DateTime.UtcNow - New DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds


        If crcpassed Then
            crcPassedCount += 1
        Else
            crcFailedCount += 1
        End If

        noMessagesCondition = False

        If comStatsHandler IsNot Nothing Then
            comStatsHandler(crcPassedCount, crcFailedCount, generalFailures, noTrafficCondition, noMessagesCondition)
        End If

        If crcpassed Then
            Select Case msgid
                Case BROADCAST_ID
                    map = parseBroadcastToolData(tooldata, tStamp)
            End Select
        End If

        map.Add(RESPONSE_ID, msgid.ToString("x4"))
        map.Add(TIMESTAMP, tStamp.ToString())
        map.Add(RECEIVED_CRC, receivedcrc.ToString("x4"))
        map.Add(CALCULATED_CRC, calculatedcrc.ToString("x4"))
        map.Add(UNIX_TIME, unixtime.ToString())

        If shouldLogToolData Then
            persistToolData(map)
        End If

        Return map
    End Function


    Public Function parseBroadcastToolData(ByVal tooldata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable

        Try
            Dim fcnid As UInt16 = extractU16(tooldata, BROADCAST_FCNID_OFFSET)

            Select Case fcnid
                Case STARTUP  ' bootup
                    map = parseBootMessage(tooldata, tsoffset)
                Case MOTION_STATUS  ' rotation
                    map = parseMotionStatusMessage(tooldata, tsoffset)
                Case FIRMWARE_VERSION ' firmware version
                    map = parseFirmwareVersionMessage(tooldata, tsoffset)
                Case HARDWARE_VERSION ' hardware version
                    map = parseHardwareVersionMessage(tooldata, tsoffset)
                Case SERIAL_NUMBER ' serial number
                    map = parseToolSerialNumber(tooldata, tsoffset)
                Case MODULE_INITIALIZED ' module initialized
                    map = parseModuleInitializedMessage(tooldata, tsoffset)
                Case GENERIC_MESSAGE ' generic message
                    map = parseGenericMessage(tooldata, tsoffset)
                Case START_MAG_SURVEY ' primary mag survey started
                    map = parsePrimaryMagSurveyStartedMessage(tooldata, tsoffset)
                Case MAG_SURVEY_DATA ' primary mag survey packet
                    map = parseMagSurveyData(tooldata, tsoffset)
                Case MAG_SURVEY_FAILED ' primary mag survey failed
                    map = parsePrimaryMagSurveyFailedMessage(tooldata, tsoffset)
                Case START_CONTINUOUS_INC_AZM
                    map = parseStartContinuousSurveyMessage(tooldata, tsoffset)
                Case CONTINUOUS_SURVEY_DATA ' continuous inc and az data
                    '                    map = parseContinuousIncAzmEx(tooldata, tsoffset)
                    map = parseContinuousIncAzm(tooldata, tsoffset)
                Case START_PRESSURE_ACQUISITION
                    map = parseStartPressureAcqMessage(tooldata, tsoffset)
                Case PRESSURE_DATA          ' pressure data
                    map = parsePressureData(tooldata, tsoffset)
                Case SHOCK_AND_VIB_DATA_STARTED
                    map = parseStartShockAndVibeStartedMessage(tooldata, tsoffset)
                Case SHOCK_AND_VIB_DATA
                    map = parseShockAndVibData(tooldata, tsoffset)
                Case SHOCK_AND_VIB_FAILED
                    map = parseShockAndVibeFailedMessage(tooldata, tsoffset)
                Case WIRELINE_VOLTAGE
                    map = parseWirelineVoltage(tooldata, tsoffset)
                Case TEMPERATURE_DATA
                    map = parseTemperature(tooldata, tsoffset)
                Case SECONDARY_MAG_SURVEY_DATA
                    map = parseSecondarayMagSurveyData(tooldata, tsoffset)
                Case START_GRYO_SURVEY
                    map = parseStartGyroAcqMessage(tooldata, tsoffset)
                Case GYRO_SURVEY_DATA
                    map = parseGyroData(tooldata, tsoffset)
                Case GYRO_SURVEY_FAILED
                    map = parseGyroSurveyFailedMessage(tooldata, tsoffset)

            End Select

            map.Add(FUNCTION_ID, fcnid.ToString("x4"))
        Catch ex As Exception
            map.Add(PARSE_ERROR, ex.ToString().Replace(vbCr, "#").Replace(vbLf, "#"))
            generalFailures += 1

            If comStatsHandler IsNot Nothing Then
                comStatsHandler(crcPassedCount, crcFailedCount, generalFailures, noTrafficCondition, noMessagesCondition)
            End If


        End Try


        Return map
    End Function

    Public Function parseMotionStatusMessage(ByRef motionstatusmessage As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim expectedsize As Integer = BROADCAST_CONTENT_OFFSET + 1 + 4 + 1

        If expectedsize = motionstatusmessage.Count Then
            Dim flowstate As Boolean = motionstatusmessage(BROADCAST_CONTENT_OFFSET) <> 0
            Dim rotationstate As Boolean = motionstatusmessage(BROADCAST_CONTENT_OFFSET + 1) <> 0
            Dim tooltype As Byte = motionstatusmessage(BROADCAST_DEVICENUM_OFFSET)
            Dim description As String = getToolDescription(tooltype)

            map.Add(FLOW_STATUS, flowstate.ToString())
            map.Add(ROTATE_STATUS, rotationstate.ToString())
            map.Add(TOOL_DEVICE_DESCRIPTION, description)
            map.Add(TOOL_DEVICE, tooltype.ToString())


            If (motionStatusDataHandler IsNot Nothing) Then
                motionStatusDataHandler(flowstate, rotationstate)
            End If

            Dim content As String = "Motion status rotation = " + rotationstate.ToString() + " flow - " + flowstate.ToString() + " tool type = " + description

            map.Add(RESPONSE_CONTENT, content)

        Else
        End If

        Return map
    End Function

    Public Function parseFirmwareVersionMessage(ByRef firmwareversionmessage As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim msg As String = extractVersion(firmwareversionmessage, BROADCAST_CONTENT_OFFSET)
        Dim tooltype As Byte = firmwareversionmessage(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)

        map.Add(DEVICE_FIRMWARE_VERSION, msg)
        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())

        If fwVersionHandler IsNot Nothing Then
            fwVersionHandler(msg, description)
        End If

        Dim content As String = "Firmware version = " + msg + " tool type = " + description

        map.Add(RESPONSE_CONTENT, content)
        Return map
    End Function

    Public Function parseHardwareVersionMessage(ByRef hardwareversionmessage As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim msg As String = extractVersion(hardwareversionmessage, BROADCAST_CONTENT_OFFSET)
        Dim tooltype As Byte = hardwareversionmessage(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)

        map.Add(DEVICE_HARDWARE_VERSION, msg)
        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())

        If hwVersionHandler IsNot Nothing Then
            hwVersionHandler(msg, description)
        End If

        Dim content As String = "Hardware version = " + msg + " tool type = " + description

        map.Add(RESPONSE_CONTENT, content)

        Return map
    End Function

    Public Function parsePrimaryMagSurveyStartedMessage(ByRef surveystarteddata As ArrayList, ByVal tsoffset As Integer) As Hashtable
 
        Return parseToolMessage(surveystarteddata, tsoffset, "Primary Mag Survey Started")
    End Function

    Public Function parsePrimaryMagSurveyFailedMessage(ByRef surveydata As ArrayList, ByVal tsoffset As Integer) As Hashtable

        Return parseToolMessage(surveydata, tsoffset, "Primary Mag Survey failed")
    End Function

    Public Function parseGyroSurveyFailedMessage(ByRef surveydata As ArrayList, ByVal tsoffset As Integer) As Hashtable

        Return parseToolMessage(surveydata, tsoffset, "Gyro Survey failed")
    End Function

    Public Function parsePrimaryGyroSurveyStartedMessage(ByRef gyrodata As ArrayList, ByVal tsoffset As Integer) As Hashtable

        Return parseToolMessage(gyrodata, tsoffset, "Primary Gyro Survey started")
    End Function

    Public Function parseModuleInitializedMessage(ByRef moduleinitialized As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim tooltype As Byte = moduleinitialized(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)
        Dim initstatus As Boolean = moduleinitialized(BROADCAST_CONTENT_OFFSET)

        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())
        map.Add(DEVICE_INITIALIZED_STATUS, initstatus.ToString())

        Dim content As String = "Module initialized = " + initstatus.ToString() + " tool type = " + description

        map.Add(RESPONSE_CONTENT, content)

        Return map
    End Function

    Public Function parseGenericMessage(ByRef genericdata As ArrayList, ByVal tsoffset As Integer) As Hashtable

        Return parseToolMessage(genericdata, tsoffset, "Generic Message")
    End Function


    Public Function parseBootMessage(ByRef bootmessage As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim tstamp As Date = Date.Now

        timeoffset = tstamp   '' timestamps are all referecenced to this time

        Return parseToolMessage(bootmessage, tsoffset, "Boot Up")
    End Function

    Public Function parseWirelineVoltage(ByRef voltagedata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim voltage As Single = extractFloat(voltagedata, BROADCAST_CONTENT_OFFSET)
        Dim tstamp As Date = timeoffset.AddSeconds(tsoffset)
        Dim tooltype As Byte = voltagedata(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)

        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())
        map.Add(WIRELINE_VOLTS, voltage.ToString())
        wirelineVoltage = voltage

        If wirelineVoltageHandler IsNot Nothing Then
            wirelineVoltageHandler(voltage, tstamp)
        End If

        Dim content As String = "Wireline Voltage V = " + voltage.ToString() + " tool type = " + description

        map.Add(RESPONSE_CONTENT, content)

        Return map
    End Function

    Public Function parseTemperature(ByRef tempdata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim annulartemp As Single = extractFloat(tempdata, BROADCAST_CONTENT_OFFSET)
        Dim drilltemp As Single = extractFloat(tempdata, BROADCAST_CONTENT_OFFSET + 4)
        Dim tstamp As Date = timeoffset.AddSeconds(tsoffset)
        Dim tooltype As Byte = tempdata(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)

        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())
        map.Add(ANNULAR_TEMPERATURE, annulartemp.ToString())
        map.Add(DRILL_TEMPERATUE, drilltemp.ToString())
        Me.annularTemperature = annulartemp
        Me.drillTemperature = drilltemp

        If temperatureHandler IsNot Nothing Then
            temperatureHandler(annulartemp, drilltemp, tstamp)
        End If

        Dim content As String = "Tool temperature Drill temp = " + drilltemp.ToString() + " Annular temp = " + annulartemp.ToString() + " tool type = " + description

        map.Add(RESPONSE_CONTENT, content)

        Return map
    End Function

    Public Function parseContinuousIncAzm(ByRef cincazmdata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim cinc As Single = extractFloat(cincazmdata, BROADCAST_CONTENT_OFFSET)
        Dim cazm As Single = extractFloat(cincazmdata, BROADCAST_CONTENT_OFFSET + 4)
        Dim tstamp As Date = timeoffset.AddSeconds(tsoffset)
        Dim tooltype As Byte = cincazmdata(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)

        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())
        map.Add(CINCLINATION, cinc.ToString())
        map.Add(CAZMIMUTH, cazm.ToString())

        If continuousSurveyHandler IsNot Nothing Then
            continuousSurveyHandler(cinc, cazm, tstamp)
        End If

        Dim content As String = "Continuous Survey Cinc = " + cinc.ToString() + " CAzm = " + cazm.ToString() + " tool:" + description

        map.Add(RESPONSE_CONTENT, content)

        Return map
    End Function

    Public Function parseContinuousIncAzmEx(ByRef cincazmdata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim tstamp As Date = timeoffset.AddSeconds(tsoffset)
        Dim tooltype As Byte = cincazmdata(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)
        Dim cinc As Single = extractFloat(cincazmdata, BROADCAST_CONTENT_OFFSET)
        Dim cazm As Single = extractFloat(cincazmdata, BROADCAST_CONTENT_OFFSET + 4)
        Dim axialshocklevel As Single = cincazmdata(BROADCAST_CONTENT_OFFSET + 8)
        Dim radialshocklevel As Single = cincazmdata(BROADCAST_CONTENT_OFFSET + 9)
        Dim axialvibelevel As Single = cincazmdata(BROADCAST_CONTENT_OFFSET + 10)
        Dim radialvibelevel As Single = cincazmdata(BROADCAST_CONTENT_OFFSET + 11)
        Dim axialvibeRMS As Single = cincazmdata(BROADCAST_CONTENT_OFFSET + 12)    '' ?? not sure what these two are
        Dim radialvibeRMS As Single = cincazmdata(BROADCAST_CONTENT_OFFSET + 13)
        Dim axialshockcount As Single = extractU16(cincazmdata, BROADCAST_CONTENT_OFFSET + 14)
        Dim radialshockcount As Single = extractU16(cincazmdata, BROADCAST_CONTENT_OFFSET + 16)
        Dim axialmaxshock As Single = cincazmdata(BROADCAST_CONTENT_OFFSET + 18)
        Dim radialmaxshock As Single = cincazmdata(BROADCAST_CONTENT_OFFSET + 19)
        Dim rpm As Single = extractFloat(cincazmdata, BROADCAST_CONTENT_OFFSET + 20)

        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())

        map.Add(AXIAL_SHOCK_LEVEL, axialshocklevel.ToString())
        map.Add(RADIAL_SHOCK_LEVEL, radialshocklevel.ToString())
        map.Add(AXIAL_VIBRATION_LEVEL, axialvibelevel.ToString())
        map.Add(RADIAL_VIBRATION_LEVEL, radialvibelevel.ToString())
        map.Add(AXIAL_VIBRATION, axialvibeRMS.ToString())
        map.Add(RADIAL_VIBRATION, radialvibeRMS.ToString())
        map.Add(AXIAL_SHOCK_COUNT, axialshockcount.ToString())
        map.Add(RADIAL_SHOCK_COUNT, radialshockcount.ToString())
        map.Add(AXIAL_SHOCK_MAX, axialmaxshock.ToString())
        map.Add(RADIAL_SHOCK_MAX, radialmaxshock.ToString())
        map.Add(TOOL_RPM, rpm.ToString())
        map.Add(CINCLINATION, cinc.ToString())
        map.Add(CAZMIMUTH, cazm.ToString())

        If continuousSurveyHandler IsNot Nothing Then
            continuousSurveyHandler(cinc, cazm, tstamp)
        End If

        If shockAndVibeHandler IsNot Nothing Then
            shockAndVibeHandler(axialshocklevel, radialshocklevel, axialvibelevel, radialvibelevel, axialvibeRMS, radialvibeRMS, axialshockcount, radialshockcount, axialmaxshock, radialmaxshock, rpm)
        End If

        Dim content As String = "Continuous Survey Cinc Ex = " + cinc.ToString() + " CAzm = " + cazm.ToString() + " tool:" + description

        map.Add(RESPONSE_CONTENT, content)

        Return map
    End Function


    Public Function parsePressureData(ByRef pressuredata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim annularpressure As Single = extractFloat(pressuredata, BROADCAST_CONTENT_OFFSET)
        Dim drillpressure As Single = extractFloat(pressuredata, BROADCAST_CONTENT_OFFSET + 4)
        Dim tstamp As Date = timeoffset.AddSeconds(tsoffset)
        Dim tooltype As Byte = pressuredata(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)

        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())
        map.Add(ANNULAR_PRESSURE, annularpressure.ToString())
        map.Add(DRILL_PRESSURE, drillpressure.ToString())
        Me.annularPressure = annularpressure
        Me.drillPressure = drillpressure

        If pressureHandler IsNot Nothing Then
            pressureHandler(annularpressure, drillpressure, tstamp)
        End If

        Dim content As String = "Pressure data annular pressure = " + annularpressure.ToString() + " drill pressure = " + drillpressure.ToString() + " tool:" + description

        map.Add(RESPONSE_CONTENT, content)

        Return map
    End Function

    Public Function parseToolSerialNumber(ByRef serialnumberdata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim serial As String = extractString(serialnumberdata, BROADCAST_CONTENT_OFFSET)
        Dim tooltype As Byte = serialnumberdata(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)

        map.Add(TOOL_SERIAL_NUMBER, serial)
        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())
        toolSerialNumber = serial

        If serialNumberHandler IsNot Nothing Then
            serialNumberHandler(serial, description)
        End If

        Dim content As String = "Tool Serial = " + serial + " tool type = " + description

        map.Add(RESPONSE_CONTENT, content)

        Return map
    End Function


    Public Function parseGyroData(ByRef gyrodata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim tooltype As Byte = gyrodata(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)
        Dim content As String = "Gyro data  tool = " + description

        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())
        map.Add(RESPONSE_CONTENT, content)
        Return map
    End Function

    Public Function parseStartShockAndVibeStartedMessage(ByRef tooldata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Return parseToolMessage(tooldata, tsoffset, "Start Dynamics Acquisition")
    End Function

    Public Function parseShockAndVibeFailedMessage(ByRef surveydata As ArrayList, ByVal tsoffset As Integer) As Hashtable
 
        Return parseToolMessage(surveydata, tsoffset, "Dynamics acquisition failed")
    End Function

    Public Function parseShockAndVibData(ByRef shockandvibdata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim axialshocklevel As Single = shockandvibdata(BROADCAST_CONTENT_OFFSET)
        Dim radialshocklevel As Single = shockandvibdata(BROADCAST_CONTENT_OFFSET + 1)
        Dim axialvibelevel As Single = shockandvibdata(BROADCAST_CONTENT_OFFSET + 2)
        Dim radialvibelevel As Single = shockandvibdata(BROADCAST_CONTENT_OFFSET + 3)
        Dim axialvibeRMS As Single = shockandvibdata(BROADCAST_CONTENT_OFFSET + 4)    '' ?? not sure what these two are
        Dim radialvibeRMS As Single = shockandvibdata(BROADCAST_CONTENT_OFFSET + 5)
        Dim axialshockcount As Single = extractU16(shockandvibdata, BROADCAST_CONTENT_OFFSET + 6)
        Dim radialshockcount As Single = extractU16(shockandvibdata, BROADCAST_CONTENT_OFFSET + 8)
        Dim axialmaxshock As Single = shockandvibdata(BROADCAST_CONTENT_OFFSET + 10)
        Dim radialmaxshock As Single = shockandvibdata(BROADCAST_CONTENT_OFFSET + 11)
        Dim rpm As Single = extractFloat(shockandvibdata, BROADCAST_CONTENT_OFFSET + 12)
        Dim tooltype As Byte = shockandvibdata(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)
        Dim content As String = "Shock and Vib data tool = " + description

        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())
        map.Add(RESPONSE_CONTENT, content)

        map.Add(AXIAL_SHOCK_LEVEL, axialshocklevel.ToString())
        map.Add(RADIAL_SHOCK_LEVEL, radialshocklevel.ToString())
        map.Add(AXIAL_VIBRATION_LEVEL, axialvibelevel.ToString())
        map.Add(RADIAL_VIBRATION_LEVEL, radialvibelevel.ToString())
        map.Add(AXIAL_VIBRATION, axialvibeRMS.ToString())
        map.Add(RADIAL_VIBRATION, radialvibeRMS.ToString())
        map.Add(AXIAL_SHOCK_COUNT, axialshockcount.ToString())
        map.Add(RADIAL_SHOCK_COUNT, radialshockcount.ToString())
        map.Add(AXIAL_SHOCK_MAX, axialmaxshock.ToString())
        map.Add(RADIAL_SHOCK_MAX, radialmaxshock.ToString())
        map.Add(TOOL_RPM, rpm.ToString())

        If shockAndVibeHandler IsNot Nothing Then
            shockAndVibeHandler(axialshocklevel, radialshocklevel, axialvibelevel, radialvibelevel, axialvibeRMS, radialvibeRMS, axialshockcount, radialshockcount, axialmaxshock, radialmaxshock, rpm)
        End If

        Return map
    End Function

    Public Function parseSecondarayMagSurveyData(ByRef magdata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable

        map = parseMagSurveyData(magdata, tsoffset)
        Return map
    End Function

    Public Function parseMagSurveyData(ByVal magdata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Dim map As New Hashtable
        Dim inc As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET)
        Dim azm As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 4)
        Dim mtf As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 8)
        Dim gtf As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 12)
        Dim totalmag As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 16)
        Dim totalgrav As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 20)
        Dim dipa As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 24)
        Dim ax As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 28)
        Dim ay As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 32)
        Dim az As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 36)
        Dim hx As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 40)
        Dim hy As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 44)
        Dim hz As Single = extractFloat(magdata, BROADCAST_CONTENT_OFFSET + 48)
        Dim tstamp As Date = timeoffset.AddSeconds(tsoffset)
        Dim tooltype As Byte = magdata(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)
        Dim content As String = "Primary data tool = " + description

        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())
        map.Add(RESPONSE_CONTENT, content)

        map.Add(INCLINATION_T, inc.ToString())
        map.Add(AZIMUTH_T, azm.ToString())
        map.Add(MAG_TOOLFACE, mtf.ToString())
        map.Add(GRAV_TOOLFACE, gtf.ToString())
        map.Add(TOTAL_MAGF, totalmag.ToString())
        map.Add(TOTAL_GRAVF, totalgrav.ToString())
        map.Add(DIP_ANGLE, dipa.ToString())
        map.Add(AX_VECTOR, ax.ToString())
        map.Add(AY_VECTOR, ay.ToString())
        map.Add(AZ_VECTOR, az.ToString())
        map.Add(MX_VECTOR, hx.ToString())
        map.Add(MY_VECTOR, hy.ToString())
        map.Add(MZ_VECTOR, hz.ToString())

        If directionalHandler IsNot Nothing Then
            directionalHandler(inc, azm, mtf, gtf, totalmag, totalgrav, dipa, tstamp)
        End If

        If sixAxisHandler IsNot Nothing Then
            sixAxisHandler(ax, ay, az, hx, hy, hz, tstamp)
        End If

        If toolDataHandler IsNot Nothing Then
            toolDataHandler(inc, azm, mtf, gtf, totalmag, totalgrav, dipa,
                            ax, ay, az, hx, hy, hz, annularPressure, drillPressure, wirelineVoltage, drillTemperature, toolSerialNumber, False, tstamp)
        End If

        Return map
    End Function

    Public Function parseStartContinuousSurveyMessage(ByRef tooldata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Return parseToolMessage(tooldata, tsoffset, "Start Continuous Survey")
    End Function

    Public Function parseStartPressureAcqMessage(ByRef tooldata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Return parseToolMessage(tooldata, tsoffset, "Start Pressure Acquisition")
    End Function

    Public Function parseStartGyroAcqMessage(ByRef tooldata As ArrayList, ByVal tsoffset As Integer) As Hashtable
        Return parseToolMessage(tooldata, tsoffset, "Start Gyro Survey")
    End Function

    Public Function parseToolMessage(ByRef tooldata As ArrayList, ByVal tsoffset As Integer, ByVal message As String) As Hashtable
        Dim map As New Hashtable
        Dim msg As String = extractString(tooldata, BROADCAST_CONTENT_OFFSET)
        Dim tooltype As Byte = tooldata(BROADCAST_DEVICENUM_OFFSET)
        Dim description As String = getToolDescription(tooltype)

        map.Add(DEVICE_GENERIC_MSG, msg)
        map.Add(TOOL_DEVICE_DESCRIPTION, description)
        map.Add(TOOL_DEVICE, tooltype.ToString())

        Dim content As String = message + " = " + msg + " tool type = " + description

        map.Add(RESPONSE_CONTENT, content)

        Return map
    End Function
    Public Function extractU16(ByVal tooldata As ArrayList, ByVal offset As Integer) As UShort
        Dim retval As UShort = 0
        Dim bytes(2) As Byte

        bytes(0) = tooldata(offset + 1)
        bytes(1) = tooldata(offset)
        retval = BitConverter.ToUInt16(bytes, 0)

        Return retval
    End Function

    Public Function extractU32(ByVal tooldata As ArrayList, ByVal offset As Integer) As UInt32
        Dim retval As UInt32 = 0
        Dim bytes(4) As Byte

        bytes(0) = tooldata(offset + 3)
        bytes(1) = tooldata(offset + 2)
        bytes(2) = tooldata(offset + 1)
        bytes(3) = tooldata(offset)
        retval = BitConverter.ToUInt32(bytes, 0)

        Return retval
    End Function

    Public Function extractU24(ByVal tooldata As ArrayList, ByVal offset As Integer) As UInt32
        Dim retval As UInt32 = 0
        Dim bytes(4) As Byte

        bytes(0) = tooldata(offset + 2)
        bytes(1) = tooldata(offset + 1)
        bytes(2) = tooldata(offset)
        bytes(3) = 0
        retval = BitConverter.ToUInt32(bytes, 0)

        Return retval
    End Function

    Public Function extractFloat(ByVal tooldata As ArrayList, ByVal offset As Integer) As Single
        Dim retval As Single = 0
        Dim bytes(4) As Byte

        bytes(0) = tooldata(offset + 3)
        bytes(1) = tooldata(offset + 2)
        bytes(2) = tooldata(offset + 1)
        bytes(3) = tooldata(offset)
        retval = BitConverter.ToSingle(bytes, 0)

        Return retval
    End Function

    Public Function extractString(ByVal tooldata As ArrayList, ByVal offset As Integer) As String
        Dim strval As String = ""
        Dim len As Integer = tooldata(offset)

        For i As Integer = 0 To len - 1
            strval += Chr(tooldata(offset + 1 + i))
        Next

        Return strval
    End Function


    Public Function getDirectionalValues(ByRef tooldata As Hashtable, ByRef inclination As Single, ByRef azimuth As Single, ByRef magtoolface As Single,
                                         ByRef gravtoolface As Single, ByRef magtotal As Single, ByRef gravtotal As Single, ByRef dipangle As Single, ByRef timestamp As Date) As Boolean
        Dim status As Boolean = False

        If tooldata.Contains(INCLINATION_T) Then
            inclination = Convert.ToSingle(tooldata(INCLINATION_T))
            azimuth = Convert.ToSingle(tooldata(AZIMUTH_T))
            magtoolface = Convert.ToSingle(tooldata(MAG_TOOLFACE))
            gravtoolface = Convert.ToSingle(tooldata(GRAV_TOOLFACE))
            magtotal = Convert.ToSingle(tooldata(TOTAL_MAGF))
            gravtotal = Convert.ToSingle(tooldata(TOTAL_GRAVF))
            dipangle = Convert.ToSingle(tooldata(DIP_ANGLE))
            status = True
        End If

        Return status
    End Function

    Public Function getSixAxisValues(ByRef tooldata As Hashtable, ByRef ax As Single, ByRef ay As Single, ByRef az As Single,
                                     ByRef mx As Single, ByRef my As Single, ByRef mz As Single, ByRef timestamp As Date) As Boolean
        Dim status As Boolean = False

        Return status
    End Function

    Public Function getContinuousSurveyData(ByRef tooldata As Hashtable, ByRef cinc As Single, ByRef cazm As Single, ByRef timestamp As Date) As Boolean
        Dim status As Boolean = False

        If tooldata.Contains(CINCLINATION) And tooldata.Contains(CAZMIMUTH) Then
            cinc = Convert.ToSingle(tooldata(CINCLINATION))
            cazm = Convert.ToSingle(tooldata(CAZMIMUTH))
            status = True
        End If

        Return status
    End Function

    Public Function getPressureData(ByRef tooldata As Hashtable, ByRef annularpressure As Single, ByRef drillpressure As Single, ByRef timestamp As Date) As Boolean
        Dim status As Boolean = False

        If tooldata.Contains(ANNULAR_PRESSURE) And tooldata.Contains(DRILL_PRESSURE) Then
            annularpressure = Convert.ToSingle(tooldata(ANNULAR_PRESSURE))
            drillpressure = Convert.ToSingle(tooldata(DRILL_PRESSURE))
            status = True
        End If


        Return status
    End Function

    Public Function getTemperatureData(ByRef tooldata As Hashtable, ByRef annulartemperature As Single, ByRef drilltemperature As Single, ByRef timestamp As Date) As Boolean
        Dim status As Boolean = False

        If tooldata.Contains(ANNULAR_TEMPERATURE) And tooldata.Contains(DRILL_TEMPERATUE) Then
            annulartemperature = Convert.ToSingle(tooldata(ANNULAR_TEMPERATURE))
            drilltemperature = Convert.ToSingle(tooldata(DRILL_TEMPERATUE))
            status = True
        End If

        Return status
    End Function

    Public Function getVoltageData(ByRef tooldata As Hashtable, ByRef wirelinevoltage As Single, ByRef timestamp As Date) As Boolean
        Dim status As Boolean = False

        If tooldata.Contains(WIRELINE_VOLTS) Then
            wirelinevoltage = Convert.ToSingle(tooldata(WIRELINE_VOLTS))
            status = True
        End If

        Return status
    End Function

    Private Sub OnTrafficCheckTimer(source As Object, e As System.Timers.ElapsedEventArgs)

        If noTrafficCondition Or noMessagesCondition Then
            If comStatsHandler IsNot Nothing Then
                comStatsHandler(crcPassedCount, crcFailedCount, generalFailures, noTrafficCondition, noMessagesCondition)
            End If
        End If

        noTrafficCondition = True
        noMessagesCondition = True
    End Sub

    Private Function getToolDescription(ByVal tooltype As Byte) As String
        Dim description As String = "undefined"

        Select Case tooltype
            Case 10
                description = "downhole controller"
            Case 30
                description = "wireline transceiver"
            Case 40
                description = "Primary Steering instrument"
            Case 70
                description = "Gyro Steering instrument"
            Case 100
                description = "Secondary Steering Instrument"
            Case 130
                description = "Pressure Module"
        End Select

        Return description
    End Function

    Private Function extractVersion(ByRef data As ArrayList, ByVal offset As Integer) As String
        Dim verstring As String = data(offset).ToString() + "." + data(offset + 1).ToString() + "." + data(offset + 2).ToString()

        Return verstring
    End Function

    Private Function persistToolData(ByRef tooldata As Hashtable)

        If Not headerWritten Then
            Dim decoratedfilename As String = Date.Now.ToString("yyyy_MMMM_dd-HH_mm_") + toolDataFileName
            headerWritten = True
            toolDataFile = New StreamWriter(decoratedfilename)

            Dim headerstr As String = ""

            For Each element As String In toolHeaderValues
                headerstr += element + ","
            Next

            toolDataFile.WriteLine(headerstr)
        End If

        Dim msg As String = ""

        For Each key As String In toolHeaderValues
            Dim value As String = ""

            If (tooldata.Contains(key)) Then
                value = tooldata(key)
            End If

            value += ","
            msg += value
        Next

        toolDataFile.WriteLine(msg)
        toolDataFile.Flush()

        Return Nothing
    End Function

End Class
