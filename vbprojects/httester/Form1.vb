﻿Public Class Form1

    Private parser As New HTToolParser

    Private Sub SerialPort1_DataReceived(sender As Object, e As IO.Ports.SerialDataReceivedEventArgs) Handles SerialPort1.DataReceived
        Dim Count As Integer = SerialPort1.BytesToRead
        Dim Buffer(0 To Count - 1) As Byte

        SerialPort1.Read(Buffer, 0, Count)

        parser.parseAndHandleToolData(Buffer)

    End Sub

    Private Function handleToolDataHashMap(ByRef data As ArrayList, ByRef map As Hashtable)
        Dim rep As String = parser.getStringRep(data)
        Dim text As String = TextBox1.Text
        Dim content As String = "Content: "
        Dim inc As Single = 0
        Dim azm As Single = 0
        Dim mtf As Single = 0
        Dim gtf As Single = 0
        Dim totalmag As Single = 0
        Dim totalgrav As Single = 0
        Dim dipa As Single = 0
        Dim tstamp As New Date
        Dim cinc As Single = 0
        Dim cazm As Single = 0
        Dim annularpressure As Single = 0
        Dim drillpressure As Single = 0


        'If map.Contains(HTToolParser.TIMESTAMP) Then
        '    content += " tstamp = " + map(HTToolParser.TIMESTAMP)
        'End If

        If map.Contains(HTToolParser.RESPONSE_ID) Then
            content += " resp id = " + map(HTToolParser.RESPONSE_ID)
        End If

        If map.Contains(HTToolParser.FUNCTION_ID) Then
            content += " fcn id = " + map(HTToolParser.FUNCTION_ID)
        End If

        If map.Contains(HTToolParser.RECEIVED_CRC) Then
            content += " rcvd crc = " + map(HTToolParser.RECEIVED_CRC)
        End If

        If map.Contains(HTToolParser.CALCULATED_CRC) Then
            content += " calculated crc = " + map(HTToolParser.CALCULATED_CRC)
        End If

        If map.Contains(HTToolParser.TIMESTAMP) Then
            content += " raw tstamp = " + map(HTToolParser.TIMESTAMP)
        End If

        If (map.Contains(HTToolParser.RESPONSE_CONTENT)) Then
            content += " content:" + map(HTToolParser.RESPONSE_CONTENT)
        End If

        If parser.getDirectionalValues(map, inc, azm, mtf, gtf, totalmag, totalgrav, dipa, tstamp) Then
            content += " inc:" + inc.ToString() + " azm:" + azm.ToString() + " mtfa:" + mtf.ToString() + " gtfa:" + gtf.ToString()
            content += " total mag:" + totalmag.ToString() + " total grav:" + totalgrav.ToString() + " dipa:" + dipa.ToString()
            content += " timestamp:" + tstamp.ToString()
        End If

        If (parser.getContinuousSurveyData(map, cinc, cazm, tstamp)) Then
            content += " cinc:" + cinc.ToString()
            content += " cazm:" + cazm.ToString()
        End If

        If (parser.getPressureData(map, annularpressure, drillpressure, tstamp)) Then
            content += " annularpressure:" + annularpressure.ToString()
            content += " drillpressure:" + drillpressure.ToString()
        End If

        If map.Contains(HTToolParser.PARSE_ERROR) Then
            content += " Parse Error = " + map(HTToolParser.PARSE_ERROR)
        End If


        text += vbCrLf + rep + vbCrLf + content

        TextBox1.Invoke(Sub()
                            TextBox1.Text = text
                            TextBox1.SelectionStart = TextBox1.Text.Length
                            TextBox1.ScrollToCaret()
                        End Sub)
        Return Nothing
    End Function


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        SerialPort1.PortName = "COM2"
        'SerialPort1.PortName = "COM3"
        SerialPort1.BaudRate = 115200
        SerialPort1.Parity = IO.Ports.Parity.None
        SerialPort1.DataBits = 8
        SerialPort1.StopBits = IO.Ports.StopBits.One
        SerialPort1.ReceivedBytesThreshold = 1

        Try
            SerialPort1.Open()
        Catch ex As Exception
            'ShowMessage("Error opening com port." & vbCrLf & ex.Message, 2)
            MsgBox(ex.Message)
        End Try

        TextBox1.Text = "Initialized"

        parser.resetState()
        '        parser.directionalHandler = AddressOf handleDirectionalData
        parser.toolDataHandler = AddressOf handleToolData
        parser.continuousSurveyHandler = AddressOf handleContinuousSurveyData
        'parser.pressureHandler = AddressOf handlePressureData
        parser.comStatsHandler = AddressOf handleCommStats
        parser.motionStatusDataHandler = AddressOf motionStatusDataHandler
        parser.shockAndVibeHandler = AddressOf shockAndVibeDataHandler
        parser.toolDataHashMapHandler = AddressOf handleToolDataHashMap

    End Sub

    Private Sub Form1_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Dim size As Size = Me.Size


        TextBox1.SetBounds(0, 0, size.Width - 20, size.Height / 2)
    End Sub

    Function handleDirectionalData(ByVal inclination As Single, ByVal azimuth As Single, ByVal magtoolface As Single,
                                         ByVal gravtoolface As Single, ByVal magtotal As Single, ByVal gravtotal As Single, ByVal dipangle As Single, ByVal timestamp As Date)
        'Dim inc As Single = inclination
        'Dim azm As Single = azimuth
        'Dim mtf As Single = magtoolface
        'Dim dipa As Single = dipangle


        incdisplay.Invoke(Sub()
                              incdisplay.Text = inclination.ToString()
                              azmdisplay.Text = azimuth.ToString()
                              mtfadisplay.Text = magtoolface.ToString()
                              dipdisplay.Text = dipangle.ToString()
                          End Sub)
        'azmdisplay.Invoke(Sub()
        '                      azmdisplay.Text = azm.ToString
        '                  End Sub)
        'mtfadisplay.Invoke(Sub()
        '                       mtfadisplay.Text = mtf.ToString
        '                   End Sub)
        Return Nothing
    End Function

    Public Function handleToolData(ByVal inclination As Single, ByVal azimuth As Single, ByVal magtoolface As Single,
                                     ByVal gravtoolface As Single, ByVal magtotal As Single, ByVal gravtotal As Single, ByVal dipangle As Single,
                                     ByVal ax As Single, ByVal ay As Single, ByVal az As Single, ByVal mx As Single, ByVal my As Single, ByVal mz As Single,
                                     ByVal annularpressure As Single, ByVal drillpressure As Single, ByVal voltage As Single, ByVal temperatrue As Single,
                                     ByVal serial As String, ByVal crcerror As Boolean, ByVal timestamp As Date)

        incdisplay.Invoke(Sub()
                              incdisplay.Text = inclination.ToString()
                              azmdisplay.Text = azimuth.ToString()
                              mtfadisplay.Text = magtoolface.ToString()
                              dipdisplay.Text = dipangle.ToString()
                              Me.annularpressure.Text = annularpressure.ToString()
                              Me.drillpressure.Text = drillpressure.ToString()
                              temperaturedisplay.Text = temperatrue.ToString()
                              voltagedisplay.Text = voltage.ToString()
                              toolserialdisplay.Text = serial
                              datedisplay.Text = timestamp.ToString()
                          End Sub)


        Return Nothing
    End Function

    Public Function handlePressureData(ByVal annularpressure As Single, ByVal drillpressure As Single, ByVal timestamp As Date)

        Me.annularpressure.Invoke(Sub()
                                      Me.annularpressure.Text = annularpressure.ToString()
                                      Me.drillpressure.Text = drillpressure.ToString()
                                  End Sub)
        Return Nothing
    End Function

    Public Function handleContinuousSurveyData(ByVal cinc As Single, ByVal cazm As Single, ByVal timestamp As Date)

        cinclination.Invoke(Sub()
                                cinclination.Text = cinc.ToString()
                                cazimuth.Text = cazm.ToString()
                            End Sub)
        Return Nothing
    End Function

    Public Function handleCommStats(ByVal goodcrccount As Integer, ByVal failedcrccount As Integer, ByVal generalfailure As Integer, ByVal notraffic As Boolean, ByVal nomessages As Boolean)

        commstats.Invoke(Sub()
                             Dim total As Integer = goodcrccount + failedcrccount
                             Dim trafficmsg As String = ""

                             If nomessages Then
                                 trafficmsg = " No Messages"
                             End If

                             If notraffic Then
                                 trafficmsg = " No traffic"
                             End If
                             commstats.Text = "Total:" + goodcrccount.ToString() + " Failed:" + failedcrccount.ToString() + " General:" + generalfailure.ToString() + trafficmsg
                         End Sub)

        Return Nothing
    End Function


    Public Function motionStatusDataHandler(flow As Boolean, rotation As Boolean)

        flowstate.Invoke(Sub()
                             Dim flowmsg As String = "Flow:" + flow.ToString()
                             Dim rotationmsg As String = "Rotation:" + rotation.ToString()

                             flowstate.Text = flowmsg
                             rotationstate.Text = rotationmsg
                         End Sub)
        Return Nothing
    End Function

    Public Function shockAndVibeDataHandler(axialshocklevel As Integer, radialshocklevel As Integer,
                                                    axialviblevel As Integer, radialviblevel As Integer,
                                                    axialviberms As Single, radialviberms As Single,
                                                    axialshockcount As Integer, radialshockcount As Integer,
                                                    axialshockmax As Single, radialshockmax As Single,
                                                    toolrpm As Single)

        Me.toolrpm.Invoke(Sub()
                              Me.toolrpm.Text = toolrpm.ToString()
                          End Sub)
        Return Nothing
    End Function
End Class
